function parsave(fname, varargin)
%PARSAVE Save for parfor
%
% parsave(fname, 'var1', var1, 'var2', var2);
%

var = struct();

for i = 1:2:numel(varargin)-1
    if ~ischar(varargin{i})
        continue;
    end
    var.(varargin{i}) = varargin{i + 1};
end

save(fname, '-struct', 'var');

end
