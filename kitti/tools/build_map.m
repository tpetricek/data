function map = build_map(base_dir, T, range, max_pts)
%BUILD_MAP Build map from velodyne point clous
%
% map = build_map(base_dir, T, range)
%
% Build map from velodyne point clous and known sensor-to-map transforms.
%
% Input:
% - base_dir
% - T         Cell array of 4-by-4 transformation matrices.
% - range     Measurement range to be used to build the map.
% Output:
% - map       3-by-N point matrix.
% 

if nargin < 4 || isempty(max_pts)
    max_pts = inf;
end
if nargin < 3 || isempty(range)
    range = [0 inf];
end
if nargin < 2 || isempty(T)
    T = load_velo_to_map(base_dir);
end
assert(isa(range, 'double'));
assert(numel(range) == 2);

map = cell(size(T));

for i = 1:numel(T)
    if isempty(T{i})
        continue;
    end
    cloud_name = sprintf('%010i.bin', i-1);
    cloud_path = [base_dir '/velodyne_points/data/' cloud_name];
    x = read_velo(cloud_path, range);
    if size(x, 2) >= max_pts
        x = x(:, randsample(size(x, 2), max_pts));
    end
    map{i} = p2e(T{i} * e2p(x));
    fprintf('Map updated with %i points from %s.\n', size(x, 2), cloud_name);
end

map = cell2mat(map);

end
