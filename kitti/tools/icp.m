function [out, p0] = icp(p0, p1, num_iters, max_dist, q_best)
%ICP Iterative closest point
%
% Input:
% - p0
% - p1
% - num_iters
% - max_dist
% - q_best
%
% Output:
% - out.T 4-by-4 transformation matrix.
% - out.error Criterion value in the last iteration.
% - p0 Transformed points, out.T * p0 ~ p1.
%

if nargin < 5
    q_best = 1;
end
if nargin < 4
    max_dist = inf;
end
if nargin < 3
    num_iters = 100;
end

p1_tree = KDTreeSearcher(p1');

derr = nan;
out = [];
for iter = 1:num_iters
    [nn, d] = p1_tree.knnsearch(p0');
    q_dist = quantile(d, q_best);
    use_mask = (d <= max_dist) & (d <= q_dist);
    out_i = dqpose(p0(1:3, use_mask), p1(:, nn(use_mask)));
    if isempty(out)
        out = out_i;
    else
        derr = 1 - out_i.error / out.error;
        out.T = out_i.T * out.T;
        out.error = out_i.error;
    end
    p0 = p2e(out_i.T * e2p(p0));
    mean_dist = mean(sqrt(sum((p1(:, nn) - p0).^2)));
    fprintf('Iter. %i/%i: crit.: %.3f (change %.3f), mean dist.: %.3f, %.2f-quantile: %.3f.\n', ...
        iter, num_iters, out_i.error, derr, mean_dist, q_best, q_dist);
    if derr < 0.001
        fprintf('Relative improvement %f < 0.001. Stop.\n', derr);
        break;
    end
end

out = rmfield(out, 'r');
out = rmfield(out, 's');

end
