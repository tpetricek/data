function out = dqpose(p0, p1, pw, n0, n1, nw, f0, f1, fw)
%DQPOSE Least-squares pose from point, normal and local frame correspondences
%
% out = dqpose(p0, p1, pw, n0, n1, nw, f0, f1, fw)
%
% The function returns transformation aligning p0 with p1, n0 with n1,
% and f0 with f1.
%
% Input:
% - p0, p1 3-by-Np (cartesian) point matrices, or 4-by-Np for quat. representation.
% - pw 1-by-Np point errors weights.
% - n0, n1 3-by-Nn surface normals, or 4-by-Nn for quat. representation.
% - nw 1-by-Nn normal errors weights.
% - f0, f1 9-by-Nf local reference frames.
% - fw local frame errors weights.
%
% Output:
% - out.T 4-by-4 transformation matrix.
% - out.r, out.s dual quaternion with two 4-by-1 components q and s.
% - out.error Error criterion value.
%

% t0 = tic();

% Assign default parameter values.
if nargin < 9 || isempty(fw)
  fw = 1;
end
if nargin < 8 || isempty(f0) || isempty(f1)
  f0 = [];
  f1 = [];
  fw = [];
end
if nargin < 6 || isempty(nw)
  nw = 1;
end
if nargin < 5 || isempty(n0) || isempty(n1)
  n0 = [];
  n1 = [];
  nw = [];
end
if nargin < 3
  pw = 1;
end
if isempty(p0) || isempty(p1)
  p0 = [];
  p1 = [];
  pw = [];
end

if isempty(p0) && isempty(n0) && isempty(f0)
  error('dqpose: Not enough inputs.');
end

% Check inputs, sizes etc.
n_points = size(p0, 2);
if numel(pw) == 1
  pw = repmat(pw, [1 n_points]);
end
if any(size(p0) ~= size(p1)) || n_points ~= size(pw, 2)
  error('Incosistent number of points and weights.');
end

n_normals = size(n0, 2);
if numel(nw) == 1
  nw = repmat(nw, [1 n_normals]);
end
if any(size(n0) ~= size(n1)) || n_normals ~= size(nw, 2)
  error('Incosistent number of normals and weights.');
end

n_frames = size(f0, 2);
if numel(fw) == 1
  fw = repmat(fw, [1 n_frames]);
end
if any(size(f0) ~= size(f1)) || n_frames ~= size(fw, 2)
  error('Incosistent number of frames and weights.');
end

% Convert inputs to quaternion representation.
if size(p0, 1) == 3
%   p0 = [1/2 * p0; zeros(1, n_points)];
  p0 = [p0; zeros(1, n_points)];
end
if size(p1, 1) == 3
%   p1 = [1/2/ * p1; zeros(1, n_points)];
  p1 = [p1; zeros(1, n_points)];
end

if size(n0, 1) == 3
  n0 = [n0; zeros(1, n_normals)];
end
if size(n1, 1) == 3
  n1 = [n1; zeros(1, n_normals)];
end

if size(f0, 1) == 9
  f_mat = f0;
  f0 = nan(4, n_frames);
  for i_frame = 1:n_frames
    f0(:, i_frame) = mat_to_quat(reshape(f_mat(:, i_frame), [3, 3]));
  end
end
if size(f1, 1) == 9
  f_mat = f1;
  f1 = nan(4, n_frames);
  for i_frame = 1:n_frames
    f1(:, i_frame) = mat_to_quat(reshape(f_mat(:, i_frame), [3, 3]));
  end
end

% Compute matrices C1, C2, C3.
C1 = zeros(4);
C2 = zeros(4);
C3 = zeros(4);
for i_point = 1:n_points
  C1 = C1 - 2 * pw(i_point) * Q(p1(:, i_point))' * W(p0(:, i_point));
  C2 = C2 + pw(i_point) * eye(4);
  C3 = C3 + 2 * pw(i_point) * (W(p0(:, i_point)) - Q(p1(:, i_point)));
end
for i_normal = 1:n_normals
  C1 = C1 - 2 * nw(i_point) * Q(n1(:, i_normal))' * W(n0(:, i_normal));
end
for i_frame = 1:n_frames
  U = rqmat(f0(:, i_frame));
  C1 = C1 - fw(i_frame) * U' * f1(:, i_frame) * f1(:, i_frame)' * U;
end

% C2 diagonal, compute inverse directly?
% C22_inv = C2;
% C22_inv(C22_inv ~= 0) =  1 ./ (2 .* C22_inv(C22_inv ~= 0));
C22_inv = pinv(C2 + C2');
A = 1/2 * (C3' * C22_inv * C3 - C1 - C1');
% [V, ~] = eig(A);
% [~, i_max] = max(diag(D));
% out.r = V(:, i_max);
[~, ~, V] = svd(A);
out.r = V(:, 1);
out.s = -C22_inv * C3 * out.r;
out.T = dq_to_mat(out.r, out.s);
out.error = out.r' * C1 * out.r + out.s' * C2 * out.s + out.s' * C3 * out.r ...
  + sum(pw .* (sum(p0.^2) + sum(p1.^2))) ...
  + 2 * sum(nw) ...
  + sum(fw);

% fprintf('dqpose: %.4f s\n', toc(t0));

  % See [Horn-1987-JOSAA] p. 633, with q = [vector, scalar]
  function Q_ = lqmat(q_)
    Q_ = [ q_(4) -q_(3)  q_(2) q_(1);
           q_(3)  q_(4) -q_(1) q_(2);
          -q_(2)  q_(1)  q_(4) q_(3);
          -q_(1) -q_(2) -q_(3) q_(4)];
  end
  function Q_ = rqmat(q_)
    Q_ = [ q_(4)  q_(3) -q_(2) q_(1);
          -q_(3)  q_(4)  q_(1) q_(2);
           q_(2) -q_(1)  q_(4) q_(3);
          -q_(1) -q_(2) -q_(3) q_(4)];
  end

  function q_ = mat_to_quat(R_)
    q_ = [nan(3, 1); (1/2) * sqrt(trace(R_) + 1)];
    if abs(q_(4)) > sqrt(eps)
      q_(1:3) = 1 / (4 * q_(4)) * [R_(3, 2) - R_(2, 3);
                                   R_(1, 3) - R_(3, 1);
                                   R_(2, 1) - R_(1, 2)];
      return;
    end
    A_ = (1 / 2) * (R_ + eye(3));
    [~, i_nz_col] = max(max(A_));
    a_ = A_(:, i_nz_col);
    q_(1:3) = a_ / norm(a_);
  end

  function R_ = quat_to_mat(q_)
    R_ = (q_(4)^2 - q_(1:3)' * q_(1:3)) * eye(3) + 2 * q_(1:3) * q_(1:3)' + 2 * q_(4) * K(q_(1:3));
  end

  function K_ = K(q_)
    K_ = [     0, -q_(3),  q_(2);
           q_(3),      0, -q_(1);
          -q_(2),  q_(1),      0];
  end

  function Q_ = Q(r_)
    Q_ = [r_(4) * eye(3) + K(r_(1:3)), r_(1:3);
                            -r_(1:3)',   r_(4)];
  end

  function W_ = W(r_)
    W_ = [r_(4) * eye(3) - K(r_(1:3)), r_(1:3);
                            -r_(1:3)',   r_(4)];
  end

  function T_ = dq_to_mat(r_, s_)
    T_ = W(r_)' * Q(r_);
%     T_(1:4, 4) = T_(1:4, 4) + 2 * W(r_)' * s_;
    T_(1:4, 4) = T_(1:4, 4) + W(r_)' * s_;
  end

end
