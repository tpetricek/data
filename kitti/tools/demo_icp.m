
close all; clear; clc;

data_dir = '2011_09_26/2011_09_26_drive_0035_sync/velodyne_points/data';

fid = fopen([data_dir '/0000000000.bin'], 'rb');
p0 = fread(fid, [4 inf], 'single');
fclose(fid);
p0 = p0(1:3, :);

fid = fopen([data_dir '/0000000001.bin'], 'rb');
p1 = fread(fid, [4 inf], 'single');
fclose(fid);
p1 = p1(1:3, :);

num_iters = 50;
max_dist = 1;
q_best = 0.5;
out = icp(p0, p1, num_iters, max_dist, q_best);

p0_1 = p2e(out.T * e2p(p0));

%%
figure('Name', 'ICP Alignment');

plot3(p0(1, :), p0(2, :), p0(3, :), 'r.'); hold on;
plot3(p1(1, :), p1(2, :), p1(3, :), 'b.'); hold on;
plot3(p0_1(1, :), p0_1(2, :), p0_1(3, :), 'g.'); hold on;

axis equal; xlabel('x'); ylabel('y'); zlabel('z');
xlim([-20, 20]);
ylim([-20, 20]);
zlim([-2, max(p0(3, :))]);
