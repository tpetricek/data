function p = plot3_color(x, rgb)
%
% p = plot3_color(x, rgb)
%
% Input:
% - x    [Nx3] point matrix.
% - rgb  [Nx3] RGB color matrix.
%
% Output:
% - p    Patch object for querying and later modification.
%
if nargin < 2
    rgb = jet(size(x, 1));
end
if size(rgb, 2) == 1
    z = rgb;
    z = z - min(z);
    z = z ./ max(z);
    cmap = colormap();
    z = round(z * (size(cmap, 1) - 1)) + 1;
    rgb = cmap(z, :);
end
assert(size(x, 2) == 3);
assert(size(rgb, 2) == 3);

% Note that patch is suprisingly much faster that plot3 with ColorOrder,
% or scatter3.
p = patch(x(:, 1), x(:, 2), x(:, 3), ...
          permute(shiftdim(rgb, -1), [2 1 3]), ...
          'FaceColor', 'none', ...
          'EdgeColor', 'none', ...
          'Marker', '.', ...
          'MarkerSize', 11, ...
          'MarkerFaceColor', 'flat', ...
          'MarkerEdgeColor', 'flat');

end
