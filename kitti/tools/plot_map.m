function fig = plot_map(fig, x, path, max_pts)
%PLOT_MAP
%
% fig = plot_map([], map, path)
% fig = plot_map(fig, map, path)
%

if nargin < 4 || isempty(max_pts)
    max_pts = inf;
end
if isa(x, 'VoxelMap')
    mode = 'isosurface';
    map = x;
    [x, ~] = get_voxels(map, [], -inf, inf);
    voxel_size = min(diff(unique(x(1,:))));
    x_lims = [min(x, [], 2) max(x, [], 2)];
    [gx, gy, gz] = ndgrid(...
        x_lims(1,1):voxel_size:x_lims(1,2), ...
        x_lims(2,1):voxel_size:x_lims(2,2), ...
        x_lims(3,1):voxel_size:x_lims(3,2));
    x = [gy(:)'; gx(:)'; gz(:)'];
    [~, val] = get_voxels(map, x);
    val(isnan(val)) = 0;
    val = reshape(val, size(gx));
%     isosurface(gy, gx, gz, val, 0, gz);
%     axis equal;
else
    mode = 'points';
end
if strcmp(mode, 'points') && size(x, 2) > max_pts
    x = x(:, randsample(size(x, 2), max_pts));
end
if nargin < 3 || isempty(path)
    path = mean(x, 2);
end

margin = [20 20 2.5]';
% z_margin = 2.5;
% xy_margin = 20;
if isempty(fig)
    fig = figure('Name', 'Map');
else
    figure(fig);
end
drawnow;

min_z = min(path(3, :)) - margin(3);
max_z = max(x(3, :));
norm_z = (x(3, :)' - min_z) / (max_z - min_z);
% norm_z = min(norm_z, 1);
% norm_z = max(norm_z, 0);

% map_rgb = [zeros(size(norm_z)) norm_z 1-norm_z];
if strcmp(mode, 'points')
%     plot3_color(x', map_rgb(norm_z, blue_green_grad(256)));
%     plot3_color(x', map_rgb(norm_z, parula(256)));
%     plot3_color(x', map_rgb(norm_z, jet(256)));
    plot3_color(x', map_rgb(norm_z, hsv(256)));
%     plot3_color(x', map_rgb(norm_z, lines(256)));
%     plot3_color(x', map_rgb(norm_z, prism(256)));
    hold on;
elseif strcmp(mode, 'isosurface')
    isosurface(gy, gx, gz, val, 0, reshape(norm_z, size(val)));
    colormap(map_rgb);
    hold on;
end

axis equal; xlabel('x'); ylabel('y'); zlabel('z');
xlim([min(path(1, :)) - margin(1), max(path(1, :)) + margin(1)]);
ylim([min(path(2, :)) - margin(2), max(path(2, :)) + margin(2)]);
zlim([min_z, max_z]);

plot3(path(1, :), path(2, :), path(3, :), 'ko-', 'LineWidth', 1);

    function rgb = blue_green_grad(n)
        r = linspace(0, 1, n)';
        rgb = [zeros(size(r)), r, 1-r];
    end

    function rgb_z = map_rgb(z, rgb)
        z = floor(max(min(z, 1-eps), 0) * size(rgb, 1)) + 1;
        rgb_z = rgb(z, :);
    end

end
