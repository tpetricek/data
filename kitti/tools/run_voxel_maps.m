% Build voxel maps for all available kitti sequences in current directory.
%% Initialize parameters.
close all; clear; clc;

free_update = -1;
hit_update = 2;
occupied_threshold = 0;

range = [2.5 100];
% max_pts = 1e3;
max_pts = inf;
voxel_sizes = [0.2 0.1];

ds = sequence_list();

%% Create maps in parallel.
for voxel_size = voxel_sizes
    parfor i_ds = 1:numel(ds)
        f = fullfile(ds{i_ds}, sprintf('voxel_map_%.2f.mat', voxel_size));
        if exist(f, 'file')
            fprintf('Map found at %s.\n', f);
            continue;
        end
        vox_map = VoxelMap(voxel_size, free_update, hit_update, occupied_threshold);
        vox_map = build_voxel_map(vox_map, ds{i_ds}, [], range, max_pts);
%         save(f, 'vox_map');
        parsave(f, 'vox_map', vox_map);
        fprintf('Voxel map saved to %s.\n', f);
    end
end

