% Demo of creating voxel map from kitti data.

%% Initialize parameters.
close all; clear; clc;

base_dir = '2011_09_26/2011_09_26_drive_0035_sync';
% base_dir = '2011_09_26/2011_09_26_drive_0036_sync';
% base_dir = '2011_09_26/2011_09_26_drive_0039_sync';

voxel_size = 0.2;
free_update = -1;
hit_update = 2;
occupied_threshold = 0;
range = [2.5 100];
max_pts = 1e4;
fig = [];

%% Load velodyne-to-map transforms.
% T = getfield(load([base_dir '/velo_to_map.mat'], 'T_icp'), 'T_icp');
T = load_velo_to_map(base_dir);
velo_path = cell2mat(cellfun(@(T) T(1:3,4), T, 'UniformOutput', false));

%% Build and show point map.
map = build_map(base_dir, T, range, max_pts);
fig(1) = figure('Name', 'Point Map');
plot_map(fig(1), map, velo_path);

%% Build and show voxel map.
vox_map = VoxelMap(voxel_size, free_update, hit_update, occupied_threshold);
vox_map = build_voxel_map(vox_map, base_dir, T, range, max_pts);
fig(2) = figure('Name', 'Voxel Map');
[x, val] = vox_map.get_voxels();
plot_map(fig(2), x, velo_path);

%% Setup 1st person camera (from git@gitlab.fel.cvut.cz:petrito1/cvm.git).
cam = cvm.FlyCam(fig);
cam.updateView();

