% Show computed ICP transforms, compare to transforms from GPS/IMU.

%% Initilize parameters.
close all; clear; clc;

% base_dir = pwd();
base_dir = '/datagrid/tradr/petrito1/workspace/data/kitti';
ds = sequence_list(base_dir);

%% 
for i_ds = 1:numel(ds)
    f = fullfile(base_dir, ds{i_ds}, 'velo_to_map.mat');
    if ~exist(f, 'file')
        continue;
    end
    velo_to_map = load(f, 'T', 'T_icp');
    
    valid_T_icp = cellfun(@(T) ~isempty(T), velo_to_map.T_icp);
    pos = cell2mat(cellfun(@(T) p2e(T*[0 0 0 1]'), velo_to_map.T, 'UniformOutput', false));
    pos_icp = cell2mat(cellfun(@(T) p2e(T*[0 0 0 1]'), velo_to_map.T_icp(valid_T_icp), 'UniformOutput', false));
    
    figure('Name', ds{i_ds});
    plot3(pos(1,:)', pos(2,:)', pos(3,:)', 'ob-');
    hold on;
    plot3(pos_icp(1,:)', pos_icp(2,:)', pos_icp(3,:)', '+r-');
    axis equal;
    xlabel('x'); ylabel('y'); zlabel('z');
    drawnow;
end
