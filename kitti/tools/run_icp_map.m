
close all; clear; clc;

ds = {'2011_09_26/2011_09_26_drive_0035_sync', ...
      '2011_09_26/2011_09_26_drive_0036_sync', ...
      '2011_09_26/2011_09_26_drive_0039_sync'};
range = [0 25];
  
for i_ds = 1:numel(ds)
    pairwise_icp_map(ds{i_ds}, range);
end
