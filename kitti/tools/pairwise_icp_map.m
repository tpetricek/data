function [map_icp, T_icp] = pairwise_icp_map(base_dir, range)
%PAIRWISE_ICP_MAP
%
% [map_icp, T_icp] = pairwise_icp_map(base_dir, range)
%
% Input:
% - base_dir    Directory containing
%               [base_dir '/oxts/data/*.txt'] and
%               [base_dir '/velodyne_points/data/*.bin'].
%
% Output:
% - map_icp     Final 3-by-N point matrix from pairwise ICP registration.
% - T_icp       A cell of 4-by-4 velodyne-to-map transformation matrix
%               from ICP registration.
% -             Velodyne-to-map transforms saved in 
%               [base_dir '/velo_to_map.mat'].
%
if nargin < 2 || isempty(range)
    range = [0 inf];
end
assert(isa(range, 'double'));
assert(numel(range) == 2);

num_iters = 25;
icp_max_dist = 1.0;
icp_map_pts = 1e7;
icp_query_pts = inf;
q_best = 0.75;

oxts = loadOxtsliteData(base_dir);
T = convertOxtsToPose(oxts);
clear oxts;

cloud_dir = [base_dir '/velodyne_points/data'];
n_clouds = numel(dir([cloud_dir '/*.bin']));
step = 1;

map_icp = cell(size(T));
T_icp = cell(size(T));
T_icp_diff = eye(4);

debug = false;
if debug
    icp_map_pts = 1e5;
    icp_query_pts = 1e3;
    num_iters = 25;
    icp_max_dist = 5;
    step = 1;
end

for i = 1:step:n_clouds
    t = tic();
    cloud_name = sprintf('%010i.bin', i-1);
    cloud_path = [cloud_dir '/' cloud_name];
    fprintf('Processing %s...\n', cloud_name);
    x = read_velo(cloud_path, range);
    % Update map with transformed points.
    % Use velo transform corrected by the previous ICP diff.
    if i <= 10  % Avoid ICP fitting velodyne capture pattarns (circles).
        T_icp{i} = T{i};
        map_icp{i} = p2e(T{i} * e2p(x));
    else
        icp_query = p2e(T_icp_diff * T{i} * e2p(x));
        if icp_query_pts < size(icp_query, 2)
            icp_query = icp_query(:, randsample(size(icp_query, 2), icp_query_pts));
        end
        icp_map = cell2mat(map_icp);
        if icp_map_pts < size(icp_map, 2);
            icp_map = icp_map(:, randsample(size(icp_map, 2), icp_map_pts));
        end
        out = icp(icp_query, icp_map, num_iters, icp_max_dist, q_best);
        T_icp_diff = out.T * T_icp_diff;
        T_icp{i} = T_icp_diff * T{i};
        map_icp{i} = p2e(T_icp{i} * e2p(x));
    end
    fprintf('%s processed: %.1f s.\n', cloud_name, toc(t));
end

map_icp = cell2mat(map_icp);

save([base_dir '/velo_to_map.mat'], 'T', 'T_icp');

end
