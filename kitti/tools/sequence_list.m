function s = sequence_list(base_dir)
%SEQUENCE_LIST List data sequence directories

if nargin < 1 || isempty(base_dir)
%     base_dir = pwd();
    base_dir = '';
end

s = strsplit(ls(fullfile(base_dir, '*/*/oxts/dataformat.txt')), '\n');
s = s(cellfun(@(n) ~isempty(n), s));
s = cellfun(@(n) fileparts(fileparts(n)), s, 'UniformOutput', false);
return;

d = dir(fullfile(base_dir, '*/*/oxts/dataformat.txt'));
s = cell(size(d));
for i = 1:numel(s)
    [dir_prefix, dir_seq] = fileparts(fileparts(d(i).folder));
    [~, dir_date] = fileparts(dir_prefix);
    s{i} = fullfile(dir_date, dir_seq);
end

end
