function tuples = collect_rgbdt(tuples_path, local_dir, rm_incomplete)
%COLLECT_RGBDT Collect RGB, depth, thermo, and mask tuples inside directory
%
% tuples = collect_rgbdt(tuples_path, local_dir, rm_incomplete)
%
% Input:
% - tuples_path Path to the tuple annotation file.
% - local_dir Directory with the data to collect, relative to the directory
%   containing tuples_path file.
%
% Output:
% - tuples Cell array of tuples, one in a row.
%

if nargin < 3 || isempty(rm_incomplete)
    rm_incomplete = true;
end
if nargin < 2 || isempty(local_dir)
    local_dir = '';
end
[tuples_dir] = fileparts(tuples_path);
data_dir = fullfile(tuples_dir, local_dir);

% Assuming RGB should be always present.
rgb_files = dir(fullfile(data_dir, 'rgb*png'));
% First outputs despite unused are not uniform.
[~, ~, ~, idx] = cellfun(@parse_rgbdt_path, {rgb_files.name}', 'UniformOutput', false);
idx = cell2mat(idx);
tuples = horzcat(...
  arrayfun(@(x) fullfile(local_dir, x.name), rgb_files, 'UniformOutput', false), ...
  arrayfun(@(i) get_name('depth_%i_*png', i), idx, 'UniformOutput', false), ...
  arrayfun(@(i) get_name('thermal_depth_%i_*png', i), idx, 'UniformOutput', false), ...
  arrayfun(@(i) get_name('mask_%i_*png', i), idx, 'UniformOutput', false));

mask_missing = cellfun(@isempty, tuples(:, 4));
fprintf('collect_rgbdt: Mask found for %.1f %% files in %s.\n', 100 * mean(~mask_missing), data_dir);
if rm_incomplete
    tuples = tuples(~mask_missing, :);
end

fid = fopen(tuples_path, 'a');
for i_tuple = 1:size(tuples, 1)
  fprintf(fid, '%s %s %s %s\n', tuples{i_tuple, :});
end
fclose(fid);

  function name = get_name(fmt, i)
    name = '';
    f = dir(fullfile(data_dir, sprintf(fmt, i)));
    if ~isempty(f)
      name = fullfile(local_dir, f(1).name);
    end
  end

end
