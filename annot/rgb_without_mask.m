function [rgb_wo_mask, rgb_w_mask] = rgb_without_mask(cp_wo_dir, cp_w_dir)

t = tic();
rgb_files = dir('rgb*');
rgb_wo_mask = rgb_files(~cellfun(@has_mask, {rgb_files.name}));
rgb_w_mask = rgb_files(cellfun(@has_mask, {rgb_files.name}));
fprintf('%i / %i RGB files without mask. (%.3f s)\n', ...
    numel(rgb_wo_mask), numel(rgb_files), toc(t));

if nargin >= 1
    mkdir(cp_wo_dir);
    for i = 1:numel(rgb_wo_mask)
        copyfile(rgb_wo_mask(i).name, fullfile(cp_wo_dir, rgb_wo_mask(i).name));
    end
end
if nargin >= 2
    mkdir(cp_w_dir);
    for i = 1:numel(rgb_w_mask)
        copyfile(rgb_w_mask(i).name, fullfile(cp_w_dir, rgb_w_mask(i).name));
    end
end

    function tf = has_mask(rgb_path)
        [~, rgb_name] = fileparts(rgb_path);
        mask_path = [strrep(rgb_name, 'rgb', 'mask') '.png'];
        tf = (exist(mask_path, 'file') == 2);
    end
end
