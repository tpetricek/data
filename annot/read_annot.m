function data = read_annot(data_path, append_data_dir)
%READ_ANNOT
%
% data = read_annot(data_path)
%

if nargin < 2
  append_data_dir = true;
end

data_dir = fileparts(data_path);
fid = fopen(data_path);
data = textscan(fid, '%s %s %s %s', 'Delimiter', ' ', 'CollectOutput', true);
data = data{1};
if append_data_dir
  data = cellfun(@(f) fullpath(f), data, 'UniformOutput', false);
end
fclose(fid);

    function p = fullpath(f)
        if isempty(f)
            p = '';
            return;
        end
        p = fullfile(data_dir, f);
    end
end
