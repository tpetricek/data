function split_rows(src, num, out)
%split_rows
%
% split_rows(src, num, out)
%
% Split rows from the input file among multiple output files.
%
% Input:
% - src Source text file with rows to split.
% - num Vector of numbers of rows in each output text file.
% - out Cell array of the same size as num, with paths to the output files.
%
% Example:
%   split_rows('annot.txt', [0.75 0.25], {'train.txt' 'test.txt'})
%

num_groups = numel(num);
if numel(out) ~= num_groups
  error('split_rows: Output paths for all groups must be specified.');
end
src_rows = importdata(src);
num_rows = numel(src_rows);

% Handle ratios.
num = floor(num ./ sum(num) * num_rows);
left = num_rows - sum(num);
while left > 0
  incr = randsample(num_groups, min(left, num_groups));
  num(incr) = num(incr) + 1;
  left = num_rows - sum(num);
end

row_perm = randperm(num_rows);

for i_group = 1:num_groups
    fid = fopen(out{i_group}, 'w');
    for i_row = 1:num(i_group)
        fprintf(fid, src_rows{row_perm(i_row)});
        fprintf(fid, '\n');
    end
    row_perm = row_perm(num(i_group) + 1:end);
    fclose(fid);
    fprintf('%i / %i rows from %s written to %s.\n', ...
        num(i_group), num_rows, src, out{i_group});
end

assert(isempty(row_perm), 'split_rows: Rows still left after processing all groups.');

end
