function write_annot(out_annot, rgbdt_tuples)
%WRITE_ANNOT Write RGBDT annotation
%
% write_annot(out_annot, rgbdt_tuples)
%

fid = fopen(out_annot, 'w');
for i = 1:size(rgbdt_tuples, 1)
  if ~isempty(rgbdt_tuples{i, 1})
    fprintf(fid, '%s %s %s %s\n', rgbdt_tuples{i, :});
  end
end
fclose(fid);

end
