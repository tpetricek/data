#!/bin/bash
#
# Mapillary Street-level Sequences Dataset
#
# The largest and most diverse dataset for lifelong place recognition from image
# sequences in urban and suburban settings.
#
# https://www.mapillary.com/dataset/places
# https://github.com/mapillary/mapillary_sls
#
# NB: Use your download links to download all archives in this directory first.
#

set -e

root="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "${root}"

wget https://www.mapillary.com/dataset/assets/mapillary-object-dataset-research-use-license-2019.pdf

# Check file integrity.
files="
images_vol_1.zip:eef296a420f6e729a5567240f71f976e
images_vol_2.zip:e1118afe32fd8db6a68bf2d72955586f
images_vol_3.zip:f20888c067c1689c1f717cd2a8f3437d
images_vol_4.zip:996b89178bf32d7eccfb65824677eb22
images_vol_5.zip:fd377d730c8183618973fadb3f998ef8
images_vol_6.zip:0129ec0b21cf53dc807658bfe781af7d
metadata.zip:4122084a9b0ca041d166f32e11599724
patch_v1.1.zip:8402279573d062282e1a473114a2f637
"
for f in ${files}; do
    IFS=":"
    set -- ${f}
    # echo $1 and $2
    name="$1"
    ref="$2"
    if [ ! -f "${name}" ]; then
        echo "File not found: ${name}."
        continue
    fi
    md5=$(md5sum $1 | cut -d " " -f 1)
    if [ "${md5}" != "${ref}" ]; then
        echo "Corrupted file: ${name}."
        break
    fi
    echo "File verified: ${name}."
    
    unzip "${name}"
done

if [ ! -d mapillary_sls ]; then
    git clone https://github.com/mapillary/mapillary_sls
else
    cd mapillary_sls
    git pull
    cd ..
fi
