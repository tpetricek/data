function in2out = label_lut(labels_in, labels_out)
%LABEL_LUT
%
% in2out = label_lut(labels_in, labels_out)
%

% Load label-to-class maps.
in2c = importdata(labels_in);
in2c = cellfun(@(s) strsplit(s, ': '), in2c, 'UniformOutput', false);
c2in = containers.Map(cellfun(@(c) c{2}, in2c, 'UniformOutput', false), cellfun(@(c) str2double(c{1}), in2c));
in2c = containers.Map(cellfun(@(c) str2double(c{1}), in2c), cellfun(@(c) c{2}, in2c, 'UniformOutput', false));

% Load label-to-class map.
out2c = importdata(labels_out);
out2c = cellfun(@(s) strsplit(s, ': '), out2c, 'UniformOutput', false);
% c2out = containers.Map(cellfun(@(c) c{2}, out2c, 'UniformOutput', false), cellfun(@(c) str2double(c{1}), out2c));
% if numel
if isscalar(out2c)
    out2c = containers.Map(str2double(out2c{1}{1}), out2c{1}{2});
else
    out2c = containers.Map(cellfun(@(c) str2double(c{1}), out2c), cellfun(@(c) c{2}, out2c, 'UniformOutput', false));
end

% Look-up table for conversion of ground truth to subset of classes (background first).
in2out = zeros([1 in2c.length()+1]);
for lout = 1:out2c.length()
    try
        lin = c2in(out2c(lout));
    catch ex
        fprintf('Key %i not found.\n', lout);
    end
    in2out(lin+1) = lout;
end

end
