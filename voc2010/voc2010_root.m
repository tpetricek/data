function root_dir = voc2010_root()
%VOC2010_ROOT VOC2010 root directory
%
% root_dir = voc2010_root()
%

root_dir = fileparts(mfilename('fullpath'));

end
