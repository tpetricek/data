function R = sfu_rotation(roll, pitch, yaw)
%SFU_ROTATION

R_x = axis_angle_rotation([1 0 0]', roll);
R_y = axis_angle_rotation([0 1 0]', pitch);
R_z = axis_angle_rotation([0 0 1]', yaw);

R = R_z * R_y * R_x;

end

function x = skew_mat(u)
x = [    0 -u(3)  u(2); ...
      u(3)     0 -u(1); ...
     -u(2)  u(1)     0];
end

function R = axis_angle_rotation(u, a)
R = cos(a) * eye(3) + sin(a) * skew_mat(u) + (1 - cos(a)) * (u * u');
end
