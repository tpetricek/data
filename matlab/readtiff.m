function x = readtiff(p)
%READTIFF Read tiff
%
% x = readtiff(p)
%
% Read a tiff file using python tifffile module.
%

x = py.tifffile.imread(p);
sz = double(py.array.array('d', x.shape));
% t = char(py.str(x.dtype));
% assert(strcmp(t, 'float32'));
x = double(py.array.array('d', x.flatten().tolist()));
x = reshape(x, flip(sz));
x = permute(x, numel(sz):-1:1);

end
