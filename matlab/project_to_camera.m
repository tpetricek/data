function [u, valid] = project_to_camera(X, K, R, t, d)
%PROJECT_TO_CAMERA
%
% [u, valid] = project_to_camera(X, K, R, t, d)
%

if nargin < 5 || isempty(d)
    d = [];
end
if nargin < 4 || isempty(t)
    t = zeros(3, 1);
end
if nargin < 3 || isempty(R)
    R = eye(3);
end

assert(size(X, 1) == 3);
assert(ismatrix(K) && all(size(K) == [3 3]));
assert(ismatrix(R) && all(size(R) == [3 3]));
assert(ismatrix(t) && all(size(t) == [3 1]));

X_cam = R * X + t;
valid = X_cam(3, :) > 0;
X_cam = X_cam(:, valid);
u = X_cam(1, :) ./ X_cam(3, :);
v = X_cam(2, :) ./ X_cam(3, :);

% Undistort if d is provided.
if ~isempty(d)
    % (k1,k2,p1,p2[,k3[,k4,k5,k6[,s1,s2,s3,s4[,tx,ty]]]]) of 4, 5, 8, 12 or 14 elements.
    d = [d(:); zeros(14 - numel(d), 1)];
    d = num2cell(d);
    [k1, k2, p1, p2, k3, k4, k5, k6, s1, s2, s3, s4, tx, ty] = d{:};
    r2 = u.^2 + v.^2;
    rb = (1 + k1 * r2 + k2 * r2.^2 + k3 * r2.^3) / (1 + k4 * r2 + k5 * r2.^2 + k6 * r2.^3);
    u2 = u .* rb + 2 * p1 .* u .* v + p2 * (r2 + 2 * u.^2) + s1 * r2 + s2 * r2.^2;
    v2 = v .* rb + p1 * (r2 + 2 * v.^2) + 2 * p2 .* u .* v + s3 * r2 + s4 * r2.^2;
%     (u - u2)
else
    u2 = u;
    v2 = v;
end

% u = K(1:2, 1:2) * [u2; v2] + K(1:2, 3);
u = nan(2, numel(valid));
u(:, valid) = K(1:2, 1:2) * [u2; v2] + K(1:2, 3);

end
