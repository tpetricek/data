function [x, t_x] = front_lidar(t_query)
%FRONT_LIDAR

persistent angles intensities ranges timestamps R t;

if isempty(timestamps)
    data = importdata('sfu_mountain/lidar_front_scan/data.csv');
    timestamps = data.data(:, 1);
    ranges = data.data(:, 2:542);
    intensities = data.data(:, 543:end);
    clear data;
%     n_scans = size(ranges, 1);
    n_points = size(ranges, 2);
    
    % Discard invalid readings.
    ranges(ranges <= 0) = nan;
    ranges(isinf(ranges)) = nan;

    % addpath(genpath('/home/petrito1/workspace/thirdparty/yamlmatlab'));
    % cal = yaml.ReadYaml('sfu_mountain/lidar_front_scan/calibration.yaml');
    R = sfu_rotation(3.14159, 0.360793, 0.0);
    t = [0.415, 0.0, 0.265]';

    angle_min = -2.35619449615;
    angle_max = 2.35619449615;
%     angle_inc = 0.00872664619237;
    % (angle_max - angle_min) / (n_pts - 1) - angle_inc
    angles = linspace(angle_min, angle_max, n_points);
end

if ischar(t_query)
    switch t_query
        case 'min'
            x = timestamps(1);
        case 'max'
            x = timestamps(end);
    end
    return;
end

[t_x, i] = closest_value(timestamps, t_query);

x = [cos(angles) .* ranges(i, :); ...
     sin(angles) .* ranges(i, :); ...
     zeros(1, numel(angles))];
x = R * x + t;

end
