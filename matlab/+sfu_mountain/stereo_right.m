function [x, t_x] = stereo_right(t_query, raw)
%STEREO_RIGHT
%
% [x, t_x] = stereo_right(t_query)
% [u, valid] = stereo_right(X)
%

if nargin < 2 || isempty(raw)
    raw = true;
end

persistent timestamps files K R t d w h;

if isempty(timestamps)
    files = dir('sfu_mountain/camera_stereo_right-0.3fps/frames/*.jpg')
%     timestamps = arrayfun(@(f) str2double(f.name), files);
    timestamps = arrayfun(@(f) str2double(file_name(f.name)), files);
    files = arrayfun(@(f) fullfile(f.folder, f.name), files, 'UniformOutput', false);
    [timestamps, i] = sort(timestamps);
    files = files(i);
    info = imfinfo(files{1});
    w = info.Width;
    h = info.Height;
    K = [370.460855, 0.0, 356.060855; 0.0, 373.109151, 244.435112; 0.0, 0.0, 1.0];
    % R = sfu_rotation(0.0, 0.0, 0.0);
    % R, t from ROS camera to base_link.
    R = [0 0 1; -1 0 0; 0 -1 0];
    t = [0.3625, -0.2065, 0.497]';
    d = [-0.256124, 0.048374, 0.000797, 0.003220, 0.0];
%     d = [];
end

if ischar(t_query)
    switch t_query
        case 'min'
            x = timestamps(1);
        case 'max'
            x = timestamps(end);
    end
    return;
end

if size(t_query, 1) == 3
%     [x, t_x] = project_to_camera(t_query, K, R, t, d);
    [x, t_x] = project_to_camera(t_query, K, R', -R'*t, d);
    t_x(any(x < -0.5) | x(1, :) > w - 0.5 | x(2, :) > h - 0.5) = false;
    % To Matlab 1-based convention?
    return;
end

[t_x, i] = closest_value(timestamps, t_query);

x = imread(files{i});

% Undistort.
if ~raw
    [xg, yg] = meshgrid(0:w - 1, 0:h - 1);
    u = K \ [xg(:)'; yg(:)'; ones(1, numel(xg))];
    assert(all(abs(u(3, :) - 1) < 1e-6));
    [u_dist, valid] = project_to_camera(u, K, eye(3), zeros(3, 1), d);
    xq = reshape(u_dist(1, :), size(xg));
    yq = reshape(u_dist(2, :), size(yg));
    x = interp_image(xg, yg, x, xq, yq);
end

end
