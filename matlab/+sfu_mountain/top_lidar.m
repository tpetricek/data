function [x, t_x] = top_lidar(t_query)
%TOP_LIDAR

persistent angles ranges timestamps R t;

if isempty(timestamps)
    data = importdata('sfu_mountain/lidar_top_scan/data.csv');
    timestamps = data.data(:, 1);
    ranges = data.data(:, 2:end);
    clear data;
%     n_scans = size(ranges, 1);
    n_points = size(ranges, 2);
    
    % Discard invalid readings.
    ranges(ranges <= 0) = nan;
    ranges(isinf(ranges)) = nan;

    % addpath(genpath('/home/petrito1/workspace/thirdparty/yamlmatlab'));
    % cal = yaml.ReadYaml('sfu_mountain/lidar_top_scan/calibration.yaml');
    R = sfu_rotation(0.0, -1.5705, 0.0);
    t = [0.215, 0.0, 0.3285]';

    angle_min = -1.56206965446;
    angle_max = 1.56206965446;
    % angle_inc = 0.0174532923847;
    % (angle_max - angle_min) / (n_pts - 1) - angle_inc
    angles = linspace(angle_min, angle_max, n_points);
end

if ischar(t_query)
    switch t_query
        case 'min'
            x = timestamps(1);
        case 'max'
            x = timestamps(end);
    end
    return;
end

[t_x, i] = closest_value(timestamps, t_query);

x = [cos(angles) .* ranges(i, :); ...
     sin(angles) .* ranges(i, :); ...
     zeros(1, numel(angles))];
x = R * x + t;

end

