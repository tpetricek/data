function a = col(A)
%COL Any array to column
%
% a = col(A)
%
% Corresponds to A(:), useful if A is an expression which cannot be indexed
% inline.
% 
% Input:
% - A Any multi-dimensional array.
%
% Output:
% - a Column representation of A.
%

a = A(:);

end
