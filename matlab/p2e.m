function x = p2e(xp, dim)
%P2E Projective to euclidean coordinates
%
% x = p2e(xp)
% x = p2e(xp, dim)
%

if nargin < 2 || isempty(dim)
  dim = find(size(xp) > 1, 1);
end

szc = num2cell(size(xp));
szc{dim} = [szc{dim} - 1, 1];
xc = mat2cell(xp, szc{:});
r = ones([1 ndims(xc{1})]);
r(dim) = size(xc{1}, dim);
x = xc{1} ./ repmat(xc{2}, r);

end
