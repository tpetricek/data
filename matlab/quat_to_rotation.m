function R = quat_to_rotation(q)
%QUAT_TO_MAT
% q = [x y z w]

assert(numel(q) == 4);

qi = col(q(1, :));
qj = col(q(2, :));
qk = col(q(3, :));
qw = col(q(4, :));

s = 1 ./ l2(q);
R = [1 - 2*s.*(qj.^2 + qk.^2), 2*s.*(qi.*qj - qk.*qw), 2*s.*(qi.*qk + qj.*qw); ...
     2*s.*(qi.*qj + qk.*qw), 1 - 2.*s*(qi.^2 + qk.^2), 2*s.*(qj.*qk - qi.*qw); ...
     2*s.*(qi.*qk - qj.*qw), 2*s.*(qj.*qk + qi.*qw),   1 - 2*s.*(qi.^2 + qj.^2)];

end

function x = col(x)
    x = x(:);
end

function n = l2(x)
    n = sum(x.^2, 1);
end
