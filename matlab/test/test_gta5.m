%% Baseline between stereo cameras
% close all; clear; clc;
seq = data.gta5.sequences(1);
pos = data.gta5.positions(seq, data.gta5.stereo(1), 1);
T_rl = data.gta5.transform(seq, {data.gta5.stereo(2) pos}, {data.gta5.stereo(1) pos})
baseline = T_rl(1, 4);
assert(abs(baseline - 0.54) <= 0.01);   % Baseline from KITTI paper
assert(all(abs(T_rl(1, 2:3)) <= 0.01)); % Movement in y and z negligible

%% Overlap of stereo clouds
close all; clear; clc;
seq = data.gta5.sequences(1);
pos = data.gta5.positions(seq, data.gta5.stereo(1), 1);
x{1} = data.gta5.depth_cloud(seq, data.gta5.stereo(1), pos);
x{1} = x{1}(:, vecnorm(x{1}) <= 50);
x{2} = data.gta5.depth_cloud(seq, data.gta5.stereo(2), pos, {data.gta5.stereo(1), pos, 'view'});
x{2} = x{2}(:, vecnorm(x{2}) <= 50);
figure;
cvm.plot3_color(x{1}', x{1}(3, :)', winter(256), 'MarkerSize', 5);
axis equal; grid on; grid minor; box on;
cvm.plot3_color(x{2}', x{2}(3, :)', autumn(256), 'MarkerSize', 5);

%% Camera origin from ndc
close all; clear; clc;
seq = data.gta5.sequences(1);
pos = data.gta5.positions(seq, data.gta5.stereo(1), 1);
T = data.gta5.transform(seq, {data.gta5.stereo(2) pos 'ndc'}, {data.gta5.stereo(1) pos 'view'})
o = p2e(T * [0 0 1 1]')

T = data.gta5.transform(seq, {data.gta5.stereo(2) pos 'view'}, {data.gta5.stereo(1) pos 'view'})
o = p2e(T * [0 0 0 1]')
