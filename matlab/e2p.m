function xp = e2p(x, dim)
%E2P Euclidean to projective coordinates
%
% xp = e2p(x)
% xp = e2p(x, dim)
%

if nargin < 2 || isempty(dim)
%   dim = find(size(x) > 1, 1);
    dim = 1;
end

sz = size(x);
sz(dim) = 1;
xp = cat(dim, x, ones(sz));

end
