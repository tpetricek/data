function fov = camera_fov(K, sz)
%CAMERA_FOV Camera field of view from camera matrix and image size
%
% fov = camera_fov(K, size)
%
% Inputs
% - K: 3-by-3 camera matrix, upper triangular.
% - sz: Image size, [height width].
%
% Outputs
% - fov: Field of view in radians, [fov_x fov_y].
%
assert(all(K(3, :) == [0 0 1]));

% cx = K(1, 3);
% cy = K(2, 3);
% 
% x = [     -0.5, cy, 1;
%      sz(2)-0.5, cy, 1]';
% x = K \ x;
% x = x ./ vecnorm(x);
% fovx = acos(x(:, 1)' * x(:, 2));
% 
% x = [cx,      -0.5, 1;
%      cx, sz(1)-0.5, 1]';
% x = K \ x;
% x = x ./ vecnorm(x);
% fovy = acos(x(:, 1)' * x(:, 2));

fovx = 2 * atan((sz(2) / 2) / K(1, 1));
fovy = 2 * atan((sz(1) / 2) / K(2, 2));
fov = [fovx fovy];
end
