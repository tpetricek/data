function x = depth(seq, camera, i, units)
%DEPTH Depth image
%
% x = depth(seq, cam, i)
%
if nargin < 4 || isempty(units)
    units = 'meters';
end
assert(ismember(units, {'ndc', 'meters'}));
if (nargin < 3 || isempty(i)) && (nargin < 2 || isempty(camera))
    % Treat seq as full path.
    p = seq;
    [seq, camera, i] = data.gta5.fileparts(p);
else
    p = [];
end

if strcmpi(units, 'meters')
    try
        if isempty(p)
            p = data.gta5.path(seq, camera, sprintf('%06i-depth.png', i));
        end
        % x = imread(p);
        x = imreadf(p, 0.001);
        return;
    catch ex
    end
end

% Fallback to reading NDC from tiff.
if isempty(p)
    p = data.gta5.path(seq, camera, sprintf('%06i-depth.tiff', i));
end
x = readtiff(p);

% To [0, 1] if NDC read from PNG; not used anymore.
if isinteger(x)
    x = double(x) / double(intmax(class(x)));
end

if strcmp(units, 'ndc')
    return;
end
info = data.gta5.camera_info(seq, camera, i);
sz = size(x);
x = p2e(info.proj_matrix(3:end, 3:end) \ e2p(x(:)'));
x = reshape(-x, sz);
end
