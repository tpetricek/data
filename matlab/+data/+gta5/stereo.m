function c = stereo(i)
%STEREO Stereo camera indices
c = [1 4];
if nargin == 1
    c = c(i);
end
end
