function pos = positions(seq, camera, idx)
%POSITIONS Positions in sequence
%
% i = positions(seq, camera)
%
paths = dir(data.gta5.path(seq, camera, '*.json'));
pos = arrayfun(@(p) str2double(file_name(p.name)), paths);
pos = sort(pos(:))';
if nargin == 3
    pos = pos(idx);
end
end
