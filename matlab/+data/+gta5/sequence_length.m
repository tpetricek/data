function len = sequence_length(seq, camera)
%SEQUENCE_LENGTH Sequence length
%
% len = sequence_length(seq, camera)
%
if nargin < 2
    camera = 0;
end
len = numel(data.gta5.json_paths(seq, camera));
end
