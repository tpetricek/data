function paths = json_paths(seq, camera)
%JSON_PATHS JSON paths
%
% paths = json_paths(seq, camera)
%
paths = dir(data.gta5.path(seq, camera, '*.json'));
paths = arrayfun(@(p) fullfile(p.folder, p.name), paths, 'UniformOutput', false);
paths = sort(paths);
end
