function p = path(seq, cam, i)
%PATH Resource path
%
% p = path(seq, cam, i)
%
assert(ischar(seq));
if nargin < 3 || isempty(i)
    i = '';
elseif ~ischar(i)
    i = sprintf('%06i', i);
end
if nargin < 2 || isempty(cam)
    cam = '';
elseif ~ischar(cam)
    cam = sprintf('%i', cam);
end
p = fullfile(data.gta5.dir(), seq, cam, i);
end
