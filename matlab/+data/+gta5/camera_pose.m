function T = camera_pose(seq, cam, i)
%CAMERA_POSE Camera pose or camera-to-world transform
%
% T = camera_pose(seq, cam, i)
%
info = data.gta5.camera_info(seq, cam, i);
T = inv(info.view_matrix);
end
