function x = depth_cloud(seq, camera, i, target, step)
%DEPTH_CLOUD
%
% x = depth_cloud(seq, cam, i, target)
%
if nargin < 5 || isempty(step)
    step = 1;
end
if nargin < 4 || isempty(target)
%     target = 'view';    % GL-like camera, look at neg z
    target = 'camera';  % CV-like camera, look at pos z
end
assert(~strcmpi(target, 'ndc'));

%-------------------------------------------------------------------------------
% z = data.gta5.depth(seq, camera, i);
z = data.gta5.depth(seq, camera, i, 'meters');
% The NDC coordinates do not depend on resolution,
% so this would work for a up/subsampled depth too.
[h, w] = size(z);
if step > 1
    z = z(1:step:end, 1:step:end);
end
% Should we map pixel centers to [-1, 1]?
% x, y = np.meshgrid(np.linspace(-1.0, 1.0, w), np.linspace(1.0, -1.0, h))
% or corners?
% [x, y] = meshgrid(linspace(-1.0 + 1 / w,  1.0 - 1 / w, w), ...
%                   linspace( 1.0 - 1 / h, -1.0 + 1 / h, h));
[y, x] = ndgrid(0:step:h - 1, 0:step:w - 1);
% if step > 1
%     x = x(1:step:end, 1:step:end);
%     y = y(1:step:end, 1:step:end);
%     z = z(1:step:end, 1:step:end);
% end
% if isinteger(z)
%     z = double(z) ./ double(intmax(class(z)));
% end
% To 3xN matrix with points in columns.
% x = [x(:)'; y(:)'; z(:)'];
% Shortcut for NDC.
% if strcmp(target, 'ndc')
%     return;
% end

P = data.gta5.camera_projection(seq, camera, i);
[K, R, t] = cvm.decompose_camera_matrix(P);
x = K \ e2p([x(:)'; y(:)']);
x = x .* z(:)';  % in camera frame
if strcmpi(target, 'camera')
    return;
end
if strcmpi(target, 'world')
    x = R' * (x - t);
    return;
end
% TODO: Any transform.
assert(false);

% Shortcut for view or world.
info = data.gta5.camera_info(seq, camera, i);
if strcmp(target, 'view')
    tf = inv(info.proj_matrix);
elseif strcmp(target, 'world')
    tf = inv(info.proj_matrix * info.view_matrix);
else
    tf = data.gta5.transform(seq, {camera i 'ndc'}, target);
end
% x = p2e(tf \ e2p(x));
x = p2e(tf * e2p(x));
end
