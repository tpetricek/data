function c = cameras(idx)
%CAMERAS
c = 0:5;
if nargin >= 1
    c = c(idx);
end
end
