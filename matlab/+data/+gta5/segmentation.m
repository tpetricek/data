function [y, l, c, p] = segmentation(stencil)
%SEGMENTATION
%
% [y, l, c, p] = segmentation(stencil)
%
%
% Output:
%   y: labels
%
% 0 background texture, all buildings and solid objects, even movable solid objects
% 1 pedestrian
% 2 vehicle
% 3 trees, bushes, grass
% 4 grass-like background texture
% 5 unknown
% 6	unknown
% 7	sky
%

y = bitand(stencil, uint8(7));

if nargout >= 2
    l = logical(bitget(stencil, 4));
end
if nargout >= 3
    c = logical(bitget(stencil, 5));
end
if nargout >= 4
    p = logical(bitget(stencil, 7));
end

end
