close all;
clear;
clc;

%% KITTI projection matrix of camera 2 (left rgb).
P2 = [7.215377e+02 0.000000e+00 6.095593e+02 4.485728e+01; ...
      0.000000e+00 7.215377e+02 1.728540e+02 2.163791e-01; ...
      0.000000e+00 0.000000e+00 1.000000e+00 2.745884e-03];
[K2, R2, t2] = cvm.decompose_camera_matrix(P2);
sz2 = [375 1242];

%% KITTI field of view.
x = K2 \ [    -0.5 sz2(2)-0.5; ...
          sz2(1)/2   sz2(1)/2; 1 1];
x = x ./ vecnorm(x);
fprintf('Horizontal FoV: %.3f\n', rad2deg(acos(x(:,1)'*x(:,2))));
x = K2 \ [sz2(2)/2   sz2(2)/2; ...
              -0.5 sz2(1)-0.5; 1 1];
x = x ./ vecnorm(x);
fprintf('Vertical FoV: %.3f\n', rad2deg(acos(x(:,1)'*x(:,2))));
x = K2 \ [-0.5 sz2(2)-0.5; ...
          -0.5 sz2(1)-0.5; 1 1];
x = x ./ vecnorm(x);
fprintf('Diagonal FoV: %.3f\n', rad2deg(acos(x(:,1)'*x(:,2))));

%% Remap images to match KITTI.
seq = data.gta5.sequences(1);
cam = data.gta5.stereo(1);
pos = data.gta5.positions(seq, cam, 400);
P1 = data.gta5.camera_projection(seq, cam, pos, [], true);
[K1, R1, t1] = cvm.decompose_camera_matrix(P1);

rgb1 = data.gta5.rgb(seq, cam, pos);
rgb2 = remap_camera(rgb1, K1, K2, sz2, 'linear', 0);
seg1 = data.gta5.segmentation(data.gta5.stencil(seq, cam, pos));
seg2 = remap_camera(seg1, K1, K2, sz2, 'nearest', 0);

figure('Name', 'Original RGB');             imshow(rgb1, 'Border', 'tight');
figure('Name', 'KITTI-like RGB');           imshow(rgb2, 'Border', 'tight');
figure('Name', 'Original Segmentation');    imshow(seg1, parula(8), 'Border', 'tight');
figure('Name', 'KITTI-like Segmentation');  imshow(seg2, parula(8), 'Border', 'tight');

%% Project points to image.
seq = data.gta5.sequences(1);
cam = data.gta5.stereo(1);
pos = data.gta5.positions(seq, cam, 1);
sz = [500 1000];
X = data.gta5.depth_cloud(seq, data.gta5.cameras(3), pos, 'world', 4);
P = data.gta5.camera_projection(seq, cam, pos, sz, true);
% [K, R, t] = data.gta5.camera_projection(seq, cam, pos);
% P = K * [R t];
X = P * e2p(X);
u = p2e(X(:, X(3, :) > 0));
figure;
imshow(imresize(data.gta5.rgb(seq, cam, pos), sz), 'Border', 'tight');
hold on;
plot(u(1, :), u(2, :), 'r.', 'MarkerSize', 3);

%% Merge point clouds into a map.
% close all; clear; clc;

use_fpv = false;
save_plots = false;

fig = figure('Name', 'Map');
ax_map = axes(fig);

pixel_step = 8;
r = 40;
min_dist = r / 2;
% min_dist = 0;
% seq = 'offroad-14';
for seq = data.gta5.sequences(6)
    seq = seq{1};

% cameras = data.gta5.cameras();  % all
% cameras = [1 2];                % left and top-down
cameras = [2];
positions = data.gta5.positions(seq, cameras(1));
% positions = [1418];
C_last = [];
for pos = positions
    for cam = cameras
        try
            P = data.gta5.camera_projection(seq, cam, pos);
        catch ex
            % Invalid image for the camera.
            continue;
        end
        [~, R, t, C] = cvm.decompose_camera_matrix(P);
        if cam == cameras(1)
            if ~isempty(C_last) && norm(C - C_last) < min_dist
%                 fprintf('Position %i skipped due to high overlap.\n', pos);
                break;
            else
                fprintf('Rendering position %s / %i / %i.\n', seq, cam, pos);
                C_last = C;
            end
        end
        
        x = data.gta5.depth_cloud(seq, cam, pos, 'camera', pixel_step);
        idx = find(vecnorm(x) <= r);
        x = x(:, idx);
        x = R' * (x - t);
        
        c = im2double(data.gta5.rgb(seq, cam, pos, pixel_step));
        c = reshape(c, [], 3);
        c = c(idx, :);
        
        cvm.plot3_color(x', c, [], 'MarkerSize', 5, 'Axes', ax_map);
        hold on;
        if pos == positions(1) && cam == cameras(1)
            axes(ax_map);
            cvm.xyzlabels();
            grid on; grid minor; box on;
            axis equal; axis tight;
        end
        drawnow;
%         pause(1);
    end
end
end

if save_plots
    set(gca, 'FontSize', 8);
    set(fig, 'PaperPositionMode', 'manual');
    set(fig, 'PaperUnits', 'centimeters');
    % set(fig, 'PaperPosition', [0 0 4.41 4.41]); % last 2 are width/height.
    % set(fig, 'PaperPosition', [0 0 8.82 8.82]); % last 2 are width/height.
    set(fig, 'PaperPosition', [0 0 30 30]); % last 2 are width/height.
    save_plot(fig, data.gta5.path(seq, 'point_map'), {'eps' 'png'});
end

% Print camera 0 path.
t = data.gta5.camera_path(seq, cameras(1), positions);
plot3(ax_map, t(1, :), t(2, :), t(3, :), 'k-');
cvm.plot3_color(t', linspace(0, 1, size(t, 2))', jet(256), 'Axes', ax_map);

if save_plots
    save_plot(fig, data.gta5.path(seq, 'point_map_path'), {'eps' 'png'});
end

if use_fpv
    axis off; grid off; box off;
    T = data.gta5.camera_pose(seq, 0, positions(1));
    fpv = cvm.FlyCam(fig);
    fpv.default.position = T(1:3, 4);
    fpv.default.lookat = T(1:3, 1);
    fpv.resetCamera();
    fpv.updateView();
end

% return;

%% Visualize segmentation from stencil buffer.
seq = data.gta5.sequences(1);
cam = data.gta5.stereo(1);
% cam = data.gta5.cameras(3);

ax_rgb = axes(figure('Name', 'RGB'));
ax_seg = axes(figure('Name', 'Segmentation'));

for pos = data.gta5.positions(seq, cam)
    fprintf('%s / %i / %i\n', seq, cam, pos);

    cla(ax_rgb);
    imshow(data.gta5.rgb(seq, cam, pos), 'Parent', ax_rgb, 'Border', 'tight');

    stencil = data.gta5.stencil(seq, cam, pos);
    y = data.gta5.segmentation(stencil);
    cla(ax_seg);
    labels = {'0 - background, buildings, objects', ...
              '1 - pedestrian', ...
              '2 - vehicle', ...
              '3 - trees, bushes, grass (traversable?)', ...
              '4 - grass-like background', '', '', ...
              '7 - sky'};
    imshow(y, jet(numel(labels)), 'Parent', ax_seg, 'Border', 'tight');
    cbar = colorbar(ax_seg);
    cbar.Ticks = (1:numel(labels)) - 0.5;
    cbar.TickLabels = labels;
    % Labels 0 and 4 for road and far background are sometimes ambiguous.
    % Mountains far ahead have 0, paved roads have sometimes 4.
    cbar.Location = 'west';
    axis equal; axis off; axis tight;

    % Bit masks
    % [~, l, c, p] = data.gta5.segmentation(stencil);
    % figure('Name', 'Artificial light source');
    % imshow(l);
    % figure('Name', 'Main character''s car');
    % imshow(c);
    % figure('Name', 'Main player character');
    % imshow(p);

    drawnow;
    pause(0.001);
end
