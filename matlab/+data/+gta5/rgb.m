function x = rgb(seq, camera, i, step)
%RGB
%
% x = RGB(seq, cam, i)
%
if nargin < 4 || isempty(step)
    step = 1;
end
% info = data.gta5.camera_info(seq, camera, i);
% p = data.gta5.path(seq, camera, [info.('imagepath') '.jpg']);
p = data.gta5.path(seq, camera, sprintf('%06i.jpg', i));
x = imread(p);
if step > 1
    x = x(1:step:end, 1:step:end, :);
end
end
