function [seq, cam, i] = fileparts(path)
%FILEPARTS File parts
%
% [seq, cam, i] = fileparts(path)
%
[dir, name] = fileparts(path);
i = str2double(name(1:6));
[dir, cam] = fileparts(dir);
cam = str2double(cam);
[~, seq] = fileparts(dir);
end
