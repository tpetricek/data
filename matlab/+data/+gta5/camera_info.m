function info = camera_info(seq, camera, i)
%CAMERA_INFO
%
% info = camera_info(seq, camera, i)
%

% paths = data.gta5.json_paths(seq, cam);
% p = paths{i + 1};
p = data.gta5.path(seq, camera, sprintf('%06i.json', i));
info = jsondecode(fileread(p));
end
