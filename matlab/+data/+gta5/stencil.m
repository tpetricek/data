function [x, y, c, p, l] = stencil(seq, camera, i)
%STENCIL
%
% x = stencil(seq, cam, i)
%
p = data.gta5.path(seq, camera, sprintf('%06i-stencil.png', i));
x = imread(p);

end
