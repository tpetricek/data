function compress_depths(path, prec)
%COMPRESS_DEPTHS Compress depth tiffs to pngs
%
% compress_depths(path, prec)
%
if nargin < 2 || isempty(prec)
    prec = 0.001;
end
for f = dir(path)'
    p0 = fullfile(f.folder, f.name);
%     x = readtiff(p);
    % Read depth in meters.
    x = data.gta5.depth(p0, [], [], 'meters');
    % Change tiff extension to png.
    [~, name] = fileparts(f.name);
    p1 = fullfile(f.folder, [name '.png']);
    imwritef(x, p1, prec);
    fprintf('%s converted to %s.\n', p0, p1);
end
end
