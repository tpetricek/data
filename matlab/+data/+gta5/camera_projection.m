function P = camera_projection(seq, cam, i, image_size, one_based)
%CAMERA_PROJECTION Camera pose or camera-to-world transform
%
% P = camera_projection(seq, cam, i, image_size, one_based)
%
% Input:
% - seq
% - cam
% - i
% - image_size [height width]; defaults to generated image resolution.
% - one_based logical, whether to use one-based pixel indices; defaults to true.
% 
if nargin < 5 || isempty(one_based)
    one_based = false;
end
one_based = logical(one_based);
info = data.gta5.camera_info(seq, cam, i);
if nargin < 4 || isempty(image_size)
    w = info.width;
    h = info.height;
else
    h = image_size(1);
    w = image_size(2);
end
D = [w/2    0 w/2 - 0.5 + one_based; ...
       0 -h/2 h/2 - 0.5 + one_based; ...
       0    0                     1];
Z = [1 0 0 0;
     0 1 0 0;
     0 0 0 1];
P = D * Z * info.proj_matrix * info.view_matrix;
end
