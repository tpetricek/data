function t = camera_path(seq, cam, i)
%CAMERA_PATH
%
% t = camera_path(seq, cam, i)
%
if nargin < 3 || isempty(i)
    i = data.gta5.positions(seq, cam);
end
t = arrayfun(@(i) cam_pos(seq, cam, i), i, 'UniformOutput', false);
t = cell2mat(t);
end

function t = cam_pos(seq, cam, i)
T = data.gta5.camera_pose(seq, cam, i);
t = T(1:3, 4);
% try
%     T = data.gta5.camera_pose(seq, cam, i);
%     t = T(1:3, 4);
% catch ex
%     t = nan(3, 1);
% end 
end
