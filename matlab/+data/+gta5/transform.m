function T = transform(seq, source, target)
%TRANSFORM Trasformation between frames
%
% T = transform(seq, target)
% T = transform(seq, source, target)
%
if nargin == 2
    target = source;
    source = 'world';
end
if ischar(source) && strcmp(source, 'world')
    % Assume target is a camera.
    info = data.gta5.camera_info(seq, target{1}, target{2});
    if numel(target) == 2 || strcmp(target{3}, 'view')
        T = info.view_matrix;
    elseif strcmp(target{3}, 'ndc')
        T = info.proj_matrix * info.view_matrix;
    end
elseif ischar(target) && strcmp(target, 'world')
    T = inv(data.gta5.transform(seq, target, source));
else
%     T = data.gta5.transform(seq, source) \ data.gta5.transform(seq, target);
%     T = inv(data.gta5.transform(seq, source)) * data.gta5.transform(seq, target);
    T = data.gta5.transform(seq, target) / data.gta5.transform(seq, source);
end
end
