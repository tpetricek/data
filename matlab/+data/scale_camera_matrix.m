function K = scale_camera_matrix(K, s)
%SCALE_CAMERA_MATRIX
%
% K = scale_camera_matrix(K, s)
%
assert(ismatrix(K) && all(size(K) == 3));
K(1, 1) = s * K(1, 1);
K(2, 2) = s * K(2, 2);
K(1, 3) = (K(1, 3) + 0.5) * s - 0.5;
K(2, 3) = (K(2, 3) + 0.5) * s - 0.5;
end
