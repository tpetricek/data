function K = camera_matrix(fov, sz)
%CAMERA_MATRIX Camera matrix from field of view and image size
%
% K = camera_matrix(fov, size)
%
% Inputs
% - fov: Field of view for the longer image edge, in radians, or [fov_x fov_y].
% - sz: Image size, scalar or [height width].
%
% Outputs
% - K: 3-by-3 camera matrix, upper triangular.
%
if isscalar(sz)
    sz = [sz sz];
end
assert(numel(sz) == 2);
if isscalar(fov)
    fx = max(sz) / tan(fov / 2) / 2;
    fy = fx;
else
    fx = sz(2) / tan(fov(1) / 2) / 2;
    fy = sz(1) / tan(fov(2) / 2) / 2;
end

h = sz(1);
w = sz(2);

K = [fx  0 w / 2 - 0.5; ...
      0 fy h / 2 - 0.5; ...
      0  0           1];
end

%{
fov = deg2rad([90 80]);
sz = [512 1024];
K = data.camera_matrix(fov, sz)
fov2 = data.camera_fov(K, sz);
rad2deg(fov2)
%}