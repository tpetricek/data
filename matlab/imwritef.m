function imwritef(img, path, prec)
%IMWRITEF Write compressed quantized floating-point image (e.g. depth)
%
%  imwritef(img, path, prec)
%
if nargin < 3 || isempty(prec)
    prec = 1;
end
assert(isscalar(prec));
assert(all(img(:) >= 0));
img = img / prec;
assert(all(img(:) < 2^32));
m = 2^16;  % uint16 values
% TODO: Higher range with more than 2 files.
d = uint16(floor(img / m)); 
r = uint16(floor(mod(img, m)));
[dir, name, ext] = fileparts(path);
assert(strcmpi(ext, '.png'));
imwrite(r, fullfile(dir, [name '-0' ext]));
imwrite(d, fullfile(dir, [name '-1' ext]));
end
