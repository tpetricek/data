function name = file_name(fpath)
%FILE_NAME File name without extension
%
% name = file_name(fpath)
%
[~, name, ~] = fileparts(fpath);
end
