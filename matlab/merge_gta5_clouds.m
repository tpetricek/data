close all;
clear;
clc;

use_fpv = false;
save_plots = false;
seq = data.gta5.sequences(1);
fig = figure();

pixel_step = 8;
r = 40;
min_dist = r / 2;
% cameras = [1 0 2 3 4 5];    % all
cameras = 2;              % top-down
% cameras = [1 4];          % stereo
% cameras = [1 2];          % left and top-down
positions = data.gta5.positions(seq, cameras(1));
t_last = [];
for pos = positions
    for cam = cameras
        try
            T = data.gta5.camera_pose(seq, cam, pos);
        catch ex
            continue;
        end
        if cam == cameras(1)
            t = T(1:3, 4);
            if ~isempty(t_last) && norm(t - t_last) <= min_dist
                fprintf('Position %i skipped due to high overlap.\n', pos);
                break;
            else
                t_last = t;
            end
        end
        
        x = data.gta5.depth_cloud(seq, cam, pos, 'view', pixel_step);
        idx = find(vecnorm(x) <= r);
%         idx = idx(randsample(numel(idx), min([numel(idx) 1e5])));
        x = x(:, idx);
        x = p2e(T * e2p(x));
        
        c = im2double(data.gta5.rgb(seq, cam, pos, pixel_step));
        c = reshape(c, [], 3);
        c = c(idx, :);
        
        % plot3(x(1, :), x(2, :), x(3, :), '.');
        cvm.plot3_color(x', c, [], 'MarkerSize', 5);
        if pos == positions(1) && cam == cameras(1)
            hold on;
            if use_fpv
                axis off; grid off; box off;
            else
                cvm.xyzlabels();
                zlabel('z');
                grid on; grid minor; box on;
            end
            axis equal; axis tight;
        end
        drawnow;
    end
end

if save_plots
    set(gca, 'FontSize', 8);
    set(fig, 'PaperPositionMode', 'manual');
    set(fig, 'PaperUnits', 'centimeters');
    % set(fig, 'PaperPosition', [0 0 4.41 4.41]); % last 2 are width/height.
    % set(fig, 'PaperPosition', [0 0 8.82 8.82]); % last 2 are width/height.
    set(fig, 'PaperPosition', [0 0 30 30]); % last 2 are width/height.
    save_plot(fig, data.gta5.path(seq, 'point_map'), {'eps' 'png'});
end

% Print camera 0 path.
t = data.gta5.camera_path(seq, cameras(1), positions);
plot3(t(1, :), t(2, :), t(3, :), 'k-');
cvm.plot3_color(t', linspace(0, 1, size(t, 2))', jet(256));

if save_plots
    save_plot(fig, data.gta5.path(seq, 'point_map_path'), {'eps' 'png'});
end

if use_fpv
    T = data.gta5.camera_pose(seq, 0, positions(1));
    fpv = cvm.FlyCam(fig);
    fpv.default.position = T(1:3, 4);
    fpv.default.lookat = T(1:3, 1);
    fpv.resetCamera();
    fpv.updateView();
end
