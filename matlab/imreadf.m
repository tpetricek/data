function img = imreadf(path, prec, centers)
%IMREADF Read compressed quantized floating-point image (e.g. depth)
%
% img = imreadf(path, prec)
%
if nargin < 3 || isempty(centers)
    centers = true;
end
if nargin < 2 || isempty(prec)
    prec = 1;
end
assert(isscalar(prec));
% TODO: Higher range with more than 2 files.
[dir, name, ext] = fileparts(path);
assert(strcmpi(ext, '.png'));
r = imread(fullfile(dir, [name '-0' ext]));
d = imread(fullfile(dir, [name '-1' ext]));
assert(isa(r, 'uint16') && isa(d, 'uint16'));
m = 2^16;  % uint16 values
img = m * double(d) + double(r);
img = img * prec;
if centers
    img(img > 0) = img(img > 0) + prec / 2;
end
end
