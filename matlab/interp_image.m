function out = interp_image(xg, yg, im, xq, yq)
%INTERP_IMAGE Inteprpolate multi-channel image
%
% out = interp_image(xg, yg, im, xq, yq)
%

% [xg, yg] = meshgrid(0:size(im, 2) - 1, 0:size(im, 1) - 1);
out = zeros(size(im), class(im));
for c = 1:size(im, 3)
    out(:, :, c) = cast(interp2(xg, yg, double(im(:, :, c)), xq, yq), class(im));
end

end
