% close all;
% clear;
% clc;

%% Camera rectification.
close all;
fig = figure();
t_0 = sfu_mountain.stereo_left('min');
t_1 = sfu_mountain.stereo_left('max');
% t = mean([t_0 t_1]);
t = t_0 + rand() * (t_1 - t_0);
im = sfu_mountain.stereo_left(t);
im_undist = sfu_mountain.stereo_left(t, false);
imshow([im im_undist]);
figure;
im_diff = vecnorm(double(im) - double(im_undist), 2, 3);
imagesc(im_diff);
hold on;
colorbar;

%% Camera toy proj
close all;
fig = figure();
t_0 = sfu_mountain.stereo_left('min');
t_1 = sfu_mountain.stereo_left('max');
t = mean([t_0 t_1]);
[left, t_left] = sfu_mountain.stereo_left(t, false);
hold off;
imshow(left);
X = [5 -1 0; 5 0 0; 5 1 0]';
u = sfu_mountain.stereo_left(X);
hold on;
plot(u(1, :), u(2, :), '.b', 'MarkerSize', 15);

%% Camera images w/ lidar projection.

fig = figure();

t_0 = sfu_mountain.stereo_left('min');
t_1 = sfu_mountain.stereo_left('max');

for t = linspace(t_0, t_1, 1000)
    
    hold off;
    [left, t_left] = sfu_mountain.stereo_left(t);
    [right, t_right] = sfu_mountain.stereo_right(t);
    imshow([left right]);
    hold on;
    set(fig, 'Name', sprintf('%.1f (%.3f)', (t_left - t_0)/1e9, (t_right - t_left)/1e9));
    
%     plot(x, y, '.');
    [x_fl, t_fl] = sfu_mountain.front_lidar(t);
%     plot3(x_fl(1, :), x_fl(2, :), x_fl(3, :), '.b');
%     hold on;

    [u_left, valid_left] = sfu_mountain.stereo_left(x_fl);
    [u_right, valid_right] = sfu_mountain.stereo_right(x_fl);
    mean(valid_left)
    mean(valid_right)
    hold on;
    % To Matlab 1-based convention + 1.
    plot(u_left(1, valid_left) + 1, u_left(2, valid_left) + 1, '.g');
    plot(u_right(1, valid_right) + size(left, 2) + 1, u_right(2, valid_right) + 1, '.g');
%     plot(u_left(1, :) + 1, u_left(2, :) + 1, '.g', 'MarkerSize', 9);
%     plot(u_right(1, :) + size(left, 2) + 1, u_right(2, :) + 1, '.g', 'MarkerSize', 9);
    
    [x_tl, t_tl] = sfu_mountain.top_lidar(t);
%     set(fig, 'Name', sprintf('%.1f (%.3f)', (t_fl - t_0)/1e9, (t_tl - t_fl)/1e9));
%     plot3(x_tl(1, :), x_tl(2, :), x_tl(3, :), '.r');
%     hold on;
    xlabel('x');
    ylabel('y');
%     zlabel('z');
    axis equal;
%     xlim([0 8]);
%     ylim([-8 8]);
%     xlim([-10 10]);
%     ylim([-10 10]);
%     zlim([-2 8]);
    grid on;
    grid minor;
    
    drawnow;
    pause(1.0);
end
return;


%%
% i = randi(n_cld);
% fig = figure('Name', sprintf('%i', i));
fig = figure();

t_0 = sfu_mountain.front_lidar('min');
t_1 = sfu_mountain.front_lidar('max');
for t = linspace(t_0, t_1, 10000)
%     set(fig, 'Name', sprintf('%.1f', (t - t_0)/1e9));
    hold off;
%     plot(x, y, '.');
    [x_fl, t_fl] = sfu_mountain.front_lidar(t);
    plot3(x_fl(1, :), x_fl(2, :), x_fl(3, :), '.b');
    hold on;
    [x_tl, t_tl] = sfu_mountain.top_lidar(t);
    set(fig, 'Name', sprintf('%.1f (%.3f)', (t_fl - t_0)/1e9, (t_tl - t_fl)/1e9));
    plot3(x_tl(1, :), x_tl(2, :), x_tl(3, :), '.r');
%     hold on;
    xlabel('x');
    ylabel('y');
    zlabel('z');
    axis equal;
%     xlim([0 8]);
%     ylim([-8 8]);
    xlim([-10 10]);
    ylim([-10 10]);
    zlim([-2 8]);
    grid on;
    grid minor;
    
    drawnow;
    pause(0.01);
end
return;



%% front lidar
data = importdata('sfu_mountain/lidar_front_scan/data.csv');
timestamps = data.data(:, 1);
ranges_fl = data.data(:, 2:542);
intensities = data.data(:, 543:end);
n_scans = size(ranges_fl, 1);
n_points = size(ranges_fl, 2);

% addpath(genpath('/home/petrito1/workspace/thirdparty/yamlmatlab'));
% cal = yaml.ReadYaml('sfu_mountain/lidar_front_scan/calibration.yaml');

% Discard invalid readings.
ranges_fl(ranges_fl == 0) = nan;
ranges_fl(isinf(ranges_fl)) = nan;

R_fl = sfu_rotation(3.14159, 0.360793, 0.0);
t_fl = [0.415, 0.0, 0.265]';

angle_min = -2.35619449615;
angle_max = 2.35619449615
angle_inc = 0.00872664619237;
% (angle_max - angle_min) / (n_pts - 1) - angle_inc
angles = linspace(angle_min, angle_max, n_points);

x_fl = [cos(angles) .* ranges(i, :); ...
        sin(angles) .* ranges(i, :); ...
        zeros(1, numel(angles))];
x_fl = R_fl * x_fl + t_fl;

%% top lidar
data = importdata('sfu_mountain/lidar_top_scan/data.csv');
timestamps = data.data(:, 1);
ranges = data.data(:, 2:end);
n_scans = size(ranges, 1);
n_points = size(ranges, 2);

% addpath(genpath('/home/petrito1/workspace/thirdparty/yamlmatlab'));
% cal = yaml.ReadYaml('sfu_mountain/lidar_top_scan/calibration.yaml');

R_fl = sfu_rotation(0.0, -1.5705, 0.0);
t_fl = [0.215, 0.0, 0.3285]';

angle_min = -1.56206965446;
angle_max = 1.56206965446;
angle_inc = 0.0174532923847;
% (angle_max - angle_min) / (n_pts - 1) - angle_inc
angles = linspace(angle_min, angle_max, n_points);

x_tl = [cos(angles) .* ranges(i, :); ...
        sin(angles) .* ranges(i, :); ...
        zeros(1, numel(angles))];

x_tl = R_fl * x_tl + t_fl;
    
%%
close all;
clc;
% i = randi(n_cld);
% fig = figure('Name', sprintf('%i', i));
fig = figure();

for i = 1:10:n_scans
    set(fig, 'Name', sprintf('%i', i));
    
    hold off;
%     plot(x, y, '.');
    plot3(x_fl(1, :), x_fl(2, :), x_fl(3, :), '.b');
    plot3(x_tl(1, :), x_tl(2, :), x_tl(3, :), '.r');
%     hold on;
    xlabel('x');
    ylabel('y');
    ylabel('z');
    axis equal;
%     xlim([0 8]);
%     ylim([-8 8]);
    xlim([-20 20]);
    ylim([-20 20]);
    grid on;
    grid minor;
    
    drawnow;
    pause(0.01);
end

%% husky odom
data = importdata('sfu_mountain/husky_odom/data.csv');
timestamps = data.data(:, 1);
linear = data.data(:, 2);
angular = data.data(:, 3);


%%
close all;
clear;
clc;

%% imu
data = importdata('sfu_mountain/imu_data/data.csv');
timestamps = data.data(:, 1);
% enu = data.data(:, 2:4);
orientation = data.data(:, 2:5);
% norm(orientation(1,:))



%% navsat enu
data = importdata('sfu_mountain/navsat_enu/data.csv');
timestamps = data.data(:, 1);
enu = data.data(:, 2:4);

% close all;
figure('Name', 'East-North-Up (GPS)');
plot3(enu(:, 1), enu(:, 2), enu(:, 3), '-');
axis equal;
xlabel('East');
ylabel('North');
zlabel('Up');

%% navsat fix
data = importdata('sfu_mountain/navsat_fix/data.csv');
timestamps = data.data(:, 1);
latitude = data.data(:, 4);
longitude = data.data(:, 5);
altitude = data.data(:, 6);

% close all;
figure('Name', 'GPS');
plot3(longitude, latitude, altitude, '-');
% axis equal;
xlabel('Longitude');
ylabel('Latitude');
zlabel('Altitude');

