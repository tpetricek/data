import data.gta5 as gta
import numpy as np
import pytest


def test_baseline():
    seq = gta.sequences[0]
    i = gta.positions(seq, gta.stereo[0])[0]
    tf_rl = gta.transform(seq, (gta.stereo[1], i), (gta.stereo[0], i))
    baseline = tf_rl[0, 3]
    # print(tf_rl)
    assert np.isclose(baseline, 0.54, atol=0.01)
    assert np.all(tf_rl[1:3, 3] <= 0.01)
