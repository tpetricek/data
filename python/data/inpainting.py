from __future__ import absolute_import, division, print_function
import cv2 as cv
import numpy as np
from scipy.spatial import cKDTree


def _shape_indices(shape):
    ij = np.meshgrid(*(np.arange(n) for n in shape), indexing='ij')
    ij = np.stack([i.ravel() for i in ij], axis=1)
    return ij


def inpaint_nearest(x, invalid, dist_type=None):
    # x = np.array()
    assert isinstance(x, np.ndarray)
    assert isinstance(invalid, np.ndarray)
    assert x.ndim - 1 <= invalid.ndim <= x.ndim
    # extra_dim = x.ndim > invalid.ndim
    # i, j = np.meshgrid(np.arange(x.shape[0]), np.arange(x.shape[1]), indexing='ij')
    # ij = np.stack((i.ravel(), j.ravel()), axis=1)
    shape = invalid.shape
    size = invalid.size
    ij = _shape_indices(shape)
    # if valid is None:
    #     valid = (x > 0).ravel()
    # else:
    #     valid = valid.ravel()
    # mask = ~valid
    invalid = invalid.ravel()
    valid = ~invalid
    index = cKDTree(ij[valid, :])
    # y = x.copy().ravel()
    # x = x.reshape((size, -1))
    y = x.copy().reshape((size, -1))
    # y = x.copy()
    d, nn = index.query(ij[invalid, :])
    # y[invalid] = x.ravel()[valid, :][nn]
    y[invalid, :] = y[valid, :][nn, :]
    y = y.reshape(x.shape)

    if dist_type is not None:
        dist = np.zeros(shape, dist_type)
        dist[invalid] = d
        return y, dist

    return y


def distance_to_valid(invalid, type=np.float32):
    assert isinstance(invalid, np.ndarray)
    assert type is not None
    shape = invalid.shape
    # ij = np.meshgrid(*(np.arange(n) for n in shape), indexing='ij')
    # ij = np.stack([i.ravel() for i in ij], axis=1)
    ij = _shape_indices(shape)
    invalid = invalid.ravel()
    valid = ~invalid
    index = cKDTree(ij[valid, :])
    d, _ = index.query(ij[invalid, :])
    if type in (np.int8, np.uint8, np.int16, np.uint16):
        d = np.clip(d, np.iinfo(type).min, np.iinfo(type).max)
    d = d.astype(type)
    dist = np.zeros(shape, type)
    dist.ravel()[invalid] = d
    # if type in (np.int8, np.uint8, np.int16, np.uint16):
    #     dist = np.clip(dist, np.iinfo(type).min, np.iinfo(type).max)
    # dist = dist.astype(type)
    return dist


def inpaint(x, invalid, type='nearest', radius=None, **kwargs):
    if type == 'nearest':
        return inpaint_nearest(x, invalid, **kwargs)
    if type == 'navier_stokes':
        return cv.inpaint(x, invalid.astype(np.uint8), radius, cv.INPAINT_NS)
    if type == 'telea':
        return cv.inpaint(x, invalid.astype(np.uint8), radius, cv.INPAINT_TELEA)
    raise ValueError('Inpainting type %s not supported.' % type)


__all__ = [
    'inpaint',
    'inpaint_nearest',
    'distance_to_valid'
]
