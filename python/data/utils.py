from timeit import default_timer as timer


def timed(fun):

    def timed_fun(*args, **kw):
        t = timer()
        result = fun(*args, **kw)
        t = timer() - t
        print('%s(%s, %s) --> %s took: %2.4f sec' % (fun.__name__, args, kw, result, t))
        return result

    return timed_fun
