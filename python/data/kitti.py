import csv
import cv2
from data.geom import inverse
from data.utils import timed
from datetime import datetime
from functools import lru_cache
import glob
from math import sin, cos, tan, log, pi
import numpy as np
import os
# from scipy.misc import imread

data_dir = os.path.realpath(os.path.join(os.path.dirname(__file__), '..', '..', 'kitti'))
data_dir = os.getenv('kitti_data_dir', data_dir)

sequences = {'city': ['2011_09_26_drive_0001_sync',
                      '2011_09_26_drive_0002_sync',
                      '2011_09_26_drive_0005_sync',
                      '2011_09_26_drive_0009_sync',
                      '2011_09_26_drive_0011_sync',
                      '2011_09_26_drive_0013_sync',
                      '2011_09_26_drive_0014_sync',
                      '2011_09_26_drive_0017_sync',
                      '2011_09_26_drive_0018_sync',
                      '2011_09_26_drive_0048_sync',
                      '2011_09_26_drive_0051_sync',
                      '2011_09_26_drive_0056_sync',
                      '2011_09_26_drive_0057_sync',
                      '2011_09_26_drive_0059_sync',
                      '2011_09_26_drive_0060_sync',
                      '2011_09_26_drive_0084_sync',
                      '2011_09_26_drive_0091_sync',
                      '2011_09_26_drive_0093_sync',
                      '2011_09_26_drive_0095_sync',
                      '2011_09_26_drive_0096_sync',
                      '2011_09_26_drive_0104_sync',
                      '2011_09_26_drive_0106_sync',
                      '2011_09_26_drive_0113_sync',
                      '2011_09_26_drive_0117_sync',
                      '2011_09_28_drive_0001_sync',
                      '2011_09_28_drive_0002_sync',
                      '2011_09_29_drive_0026_sync',
                      '2011_09_29_drive_0071_sync'],
             'residential': ['2011_09_26_drive_0019_sync',
                             '2011_09_26_drive_0020_sync',
                             '2011_09_26_drive_0022_sync',
                             '2011_09_26_drive_0023_sync',
                             '2011_09_26_drive_0035_sync',
                             '2011_09_26_drive_0036_sync',
                             '2011_09_26_drive_0039_sync',
                             '2011_09_26_drive_0046_sync',
                             '2011_09_26_drive_0061_sync',
                             '2011_09_26_drive_0064_sync',
                             '2011_09_26_drive_0079_sync',
                             '2011_09_26_drive_0086_sync',
                             '2011_09_26_drive_0087_sync',
                             '2011_09_30_drive_0018_sync',
                             '2011_09_30_drive_0020_sync',
                             '2011_09_30_drive_0027_sync',
                             '2011_09_30_drive_0028_sync',
                             '2011_09_30_drive_0033_sync',
                             '2011_09_30_drive_0034_sync',
                             '2011_10_03_drive_0027_sync',
                             '2011_10_03_drive_0034_sync'],
             'road': ['2011_09_26_drive_0015_sync',
                      '2011_09_26_drive_0027_sync',
                      '2011_09_26_drive_0028_sync',
                      '2011_09_26_drive_0029_sync',
                      '2011_09_26_drive_0032_sync',
                      '2011_09_26_drive_0052_sync',
                      '2011_09_26_drive_0070_sync',
                      '2011_09_26_drive_0101_sync',
                      '2011_09_29_drive_0004_sync',
                      '2011_09_30_drive_0016_sync',
                      '2011_10_03_drive_0042_sync',
                      '2011_10_03_drive_0047_sync'],
             'campus': ['2011_09_28_drive_0016_sync',
                        '2011_09_28_drive_0021_sync',
                        '2011_09_28_drive_0034_sync',
                        '2011_09_28_drive_0035_sync',
                        '2011_09_28_drive_0037_sync',
                        '2011_09_28_drive_0038_sync',
                        '2011_09_28_drive_0039_sync',
                        '2011_09_28_drive_0043_sync',
                        '2011_09_28_drive_0045_sync',
                        '2011_09_28_drive_0047_sync']}
categories = sorted(sequences)
sequences['all'] = sum(sequences.values(), [])
cameras = [0, 1, 2, 3]
imu_fields = ('lat', 'lon', 'alt',
              'roll', 'pitch', 'yaw',
              'vn', 've', 'vf', 'vl', 'vu',
              'ax', 'ay', 'ay', 'af', 'al', 'au',
              'wx', 'wy', 'wz', 'wf', 'wl', 'wu',
              'pos_accuracy', 'vel_accuracy',
              'navstat', 'numsats', 'posmode', 'velmode', 'orimode')


def sequence_dir(seq):
    """Sequence directory path.
    :param str seq: Sequence name.
    :return str: Sequence directory path.
    >>> os.path.normpath(sequence_dir(sequences['city'][0])).split(os.sep)[-2:]
    ['2011_09_26', '2011_09_26_drive_0001_sync']
    """
    return os.path.join(data_dir, seq[0:10], seq)


@lru_cache()
def sequence_length(seq):
    """Sequence length.
    :param str seq: Sequence name.
    :return int: Sequence length.
    """
    # velo_files = glob.glob(os.path.join(data_dir, seq, 'velodyne_points', 'data', '*.bin'))
    # imu_files = glob.glob(os.path.join(data_dir, seq, 'oxts', 'data', '*.txt'))
    velo_files = glob.glob(os.path.join(sequence_dir(seq), 'velodyne_points', 'data', '*.bin'))
    imu_files = glob.glob(os.path.join(sequence_dir(seq), 'oxts', 'data', '*.txt'))
    # assert len(velo_files) == len(imu_files), 'Inconsistent number of Velodyne and IMU data in %s.' % seq
    if len(velo_files) != len(imu_files):
        print('Inconsistent number of Velodyne (%i) and IMU (%i) data in %s.' % (len(velo_files), len(imu_files), seq))
    return max((len(velo_files), len(imu_files)))


def sequence_positions(seq=None, start=0, stop=None, step=1):
    """Sequence position indices.
    :param str seq: Sequence name.
    :param int start: Start index (included).
    :param int stop: Stop index (excluded).
    :param int step: Position step.
    :return: Sequence position indices (an iterable).
    """
    if seq is None:
        # seq = chain(*(sequence_positions(s, start, stop, step) for s in sequences['all']))
        # return seq
        for s in sequences['all']:
            for p in sequence_positions(s, start, stop, step):
                yield p
        return
    if stop is None:
        stop = sequence_length(seq)
    else:
        stop = min((stop, sequence_length(seq)))
    # return ((seq, i) for i in range(start, stop, step))
    # return [(seq, i) for i in range(start, stop, step)]
    for i in range(start, stop, step):
        yield seq, i


def calibration_path(seq, source, target):
    """Calibration file path."""
    return os.path.join(sequence_dir(seq), '..', 'calib_%s_to_%s.txt' % (source, target))


# @timed
@lru_cache()
def timestamps(fpath):
    """Read timestamps from file.
    :param str fpath: File path.
    :return list: Timestamps.
    """
    fmt = '%Y-%m-%d %H:%M:%S.%f'
    with open(fpath, 'r') as f:
        dt = [datetime.strptime(line[:-3], fmt) for line in f.read().splitlines()]
    return dt


@lru_cache()
def calibration(fpath):
    """Read calibration from file.
    :param str fpath: File path.
    :return numpy.matrix: Calibration.
    """
    with open(fpath, 'r') as f:
        s = f.read()
    i_r = s.index('R:')
    i_t = s.index('T:')
    rotation = np.mat(s[i_r+2:i_t], dtype=np.float64).reshape((3, 3))
    translation = np.mat(s[i_t + 2:], dtype=np.float64).reshape((3, 1))
    transform = np.bmat([[rotation, translation], [[[0, 0, 0, 1]]]])
    return transform


def calibration_values(path, key):
    with open(path, 'r', encoding='utf-8') as file:
        for row in csv.reader(file, delimiter=' '):
            if row[0] == key + ':':
                return row[1:]
    raise ValueError('Key not found.')


def projection_matrix(seq, camera, i=None, factorized=False):
    if camera not in cameras:
        raise ValueError('Invalid camera given.')
    if i is not None:
        raise NotImplementedError('Creating general camera projection matrix '
                                  'not implemented.')
    path = os.path.join(sequence_dir(seq), '..', 'calib_cam_to_cam.txt')
    key = 'P_rect_%02i' % int(camera)
    P = np.array([float(s) for s in calibration_values(path, key)]).reshape([3, 4])
    if factorized:
        return cv2.decomposeProjectionMatrix(P)
    return P


def image_size(seq, camera):
    path = os.path.join(sequence_dir(seq), '..', 'calib_cam_to_cam.txt')
    key = 'S_rect_%02i' % int(camera)
    sz = tuple(int(float(s)) for s in calibration_values(path, key))
    return sz[-1::-1]


@lru_cache()
def imu_to_laser(seq):
    """Transform from IMU to Velodyne.
    :param str seq: Sequence name.
    :return numpy.matrix: Transform.

    >>> imu_to_laser(sequences['city'][0]).shape
    (4, 4)
    """
    return calibration(calibration_path(seq, 'imu', 'velo'))


@lru_cache()
def laser_to_camera(seq):
    """Transform from Velodyne to camera.
    :param str seq: Sequence name.
    :return numpy.matrix: Transform.
    """
    return calibration(calibration_path(seq, 'velo', 'cam'))


def imu_path(seq, i):
    """IMU data file path.
    :param str seq: Sequence name.
    :param int i: Position index.
    :return str: Path to IMU data.
    """
    return os.path.join(sequence_dir(seq), 'oxts', 'data', '%010i.txt' % i)


def imu_data(seq, i):
    """Read IMU data.
    :param str seq: Sequence name.
    :param int i: Position index.
    :return dict: IMU data.
    """
    with open(imu_path(seq, i), 'rb') as f:
        return dict(zip(imu_fields, (float(x) for x in f.read().split())))


def imu_data_to_pose(data, data_0):
    assert data is not None
    scale = cos(data_0['lat'] * pi / 180.0)
    er = 6378137
    tx = scale * data['lon'] * pi * er / 180.0
    ty = scale * er * log(tan((90.0 + data['lat']) * pi / 360.0))
    tz = data['alt']
    translation = np.matrix([[tx], [ty], [tz]], dtype=np.float64)
    rx, ry, rz = data['roll'], data['pitch'], data['yaw']
    rx_mat = np.matrix([[1, 0, 0], [0, cos(rx), -sin(rx)], [0, sin(rx), cos(rx)]], dtype=np.float64)
    ry_mat = np.matrix([[cos(ry), 0, sin(ry)], [0, 1, 0], [-sin(ry), 0, cos(ry)]], dtype=np.float64)
    rz_mat = np.matrix([[cos(rz), -sin(rz), 0], [sin(rz), cos(rz), 0], [0, 0, 1]], dtype=np.float64)
    rotation = rz_mat * ry_mat * rx_mat
    transform = np.bmat([[rotation, translation], [[[0, 0, 0, 1]]]])
    return transform


@lru_cache()
def imu_pose(seq, i):
    """IMU pose (transformation from IMU to world).
    :param str seq: Sequence name.
    :param int i: Position index.
    :return numpy.matrix: IMU pose.
    """
    return imu_data_to_pose(imu_data(seq, i), imu_data(seq, 0))


def imu_stamps(seq, start=0, stop=None, step=1):
    # TODO: Support seq_name / pos format.
    if isinstance(seq, str):
        seq = list(sequence_positions(seq, start, stop, step))
    else:
        seq = list(seq)
    stamps = timestamps(os.path.join(data_dir, sequence_dir(seq[0][0]), 'oxts', 'timestamps.txt'))
    # return stamps
    return (stamps[i] for _, i in seq)


def check_camera(camera):
    """Check that the camera is valid.
    :param int camera: Camera index.
    """
    assert camera in cameras, 'Invalid camera: %i.' % camera


def image_path(seq, i, camera=0):
    """Image path.
    :param str seq: Sequence name.
    :param int i: Position index.
    :param int camera: Camera index.
    :return str: Path to image.
    """
    check_camera(camera)
    return os.path.join(sequence_dir(seq), 'image_%02i' % camera, 'data', '%010i.png' % i)


def image(seq, i, camera=0):
    """Read image.
    :param str seq: Sequence name.
    :param int i: Position index.
    :param int camera: Camera index.
    :return numpy.array: Image.
    """
    check_camera(camera)
    # return imread(image_path(seq, i, camera))
    return cv2.imread(image_path(seq, i, camera), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH)


def images(seq, camera=0, start=0, stop=None, step=1):
    """Images from sequence.
    :param str seq: Sequence name.
    :param int camera: Camera index.
    :return generator: Images.
    """
    if isinstance(seq, str):
        seq = sequence_positions(seq, start, stop, step)
    check_camera(camera)
    return (image(s, i, camera) for s, i in seq)


def image_stamps(seq, camera=0, start=0, stop=None, step=1):
    """Image timestamps from sequence.
    :param str seq: Sequence name.
    :param int camera: Camera index.
    :return generator: Image timestamps.
    """
    if isinstance(seq, str):
        seq = sequence_positions(seq, start, stop, step)
    check_camera(camera)
    # stamps = []
    for s, i in seq:
        fpath = os.path.join(data_dir, seq, 'image_%02i' % camera, 'timestamps.txt')
        yield timestamps(fpath)[i]
    # return timestamps()


def laser_scan_path(seq, i):
    """Velodyne file path.
    :param str seq: Sequence name.
    :param int i: Position index.
    :return str: Path to Velodyne cloud.
    """
    return os.path.join(sequence_dir(seq), 'velodyne_points', 'data', '%010i.bin' % i)


def laser_scan(seq, i):
    """Read Velodyne cloud.
    :param str seq: Sequence name.
    :param int i: Position index.
    :return numpy.array: Velodyne cloud.
    """
    try:
        with open(laser_scan_path(seq, i), 'rb') as f:
            # return np.fromfile(f, dtype=np.float32).reshape((-1, 4))
            return np.fromfile(f, dtype=np.float32).reshape((-1, 4))[:, 0:3].T
    except FileNotFoundError as err:
        if i < sequence_length(seq):
            print('Scan not available: %s, %i. Returning None.' % (seq, i))
            return None
        else:
            raise


def laser_scans(seq, start=0, stop=None, step=1):
    """Velodyne point clouds from sequence.
    :param str seq: Sequence name or positions.
    :return generator: Velodyne point clouds.

    >>> len(list(laser_scans(sequences['city'][0])))
    108
    """
    if isinstance(seq, str):
        seq = sequence_positions(seq, start, stop, step)
    # return (laser_scan(seq, i) for i in sequence_positions(seq, start, stop, step))
    for s, i in seq:
        yield laser_scan(s, i)


def laser_scan_stamps(seq):
    """Velodyne timestamps from sequence.
    :param str seq: Sequence name.
    :return generator: Velodyne timestamps.

    >>> len(list(laser_scan_stamps(sequences['city'][0])))
    108
    """
    # TODO: Sequence positions as input.
    return timestamps(os.path.join(sequence_dir(seq), 'velodyne_points', 'timestamps.txt'))


def laser_scan_start_stamps(seq):
    # TODO: Sequence positions as input.
    return timestamps(os.path.join(sequence_dir(seq), 'velodyne_points', 'timestamps_start.txt'))


def laser_scan_end_stamps(seq):
    # TODO: Sequence positions as input.
    return timestamps(os.path.join(sequence_dir(seq), 'velodyne_points', 'timestamps_end.txt'))


@lru_cache()
def transform(seq, source, target, i_source=None, i_target=None):
    """Transform between frames and positions (or, frame poses).
    :param seq: Sequence name.
    :param source: Source (child) frame.
    :param target: Target (reference) frame.
    :param i_source: Source position (time).
    :param i_target: Target position (time).
    :return numpy.matrix: Transform.
    """
    if i_target is None:
        i_target = i_source
    # Handle same position index first.
    if i_source == i_target:
        if source == target:
            return np.matrix(np.eye(4), copy=False)
        elif (source, target) == ('imu', 'laser'):
            return imu_to_laser(seq)
        elif (source, target) == ('laser', 'imu'):
            return inverse(imu_to_laser(seq))
        elif (source, target) == ('laser', 'camera'):
            return laser_to_camera(seq)
        elif (source, target) == ('camera', 'laser'):
            return inverse(laser_to_camera(seq))
        elif (source, target) == ('imu', 'world'):
            return imu_pose(seq, i_source)
        elif (source, target) == ('world', 'imu'):
            return inverse(imu_pose(seq, i_source))
        raise ValueError('Invalid frame pair: (%s, %s).' % (source, target))
    assert i_target is not None
    # Handle different positions (transform through world).
    return (transform(seq, 'imu', target, i_target)
            * transform(seq, 'world', 'imu', i_target)
            * transform(seq, 'imu', 'world', i_source)
            * transform(seq, source, 'imu', i_source))


# def transforms(seq, source, target=None, i_target=None, start=0, stop=None, step=1):
#     if target is None:
#         target = source
#     if i_target is None:
#         i_target = start
#     return (transform(seq, source, target, i, i_target) for i in sequence_positions(seq, start, stop, step))


def transforms(seq, source, target=None, i_target=None, start=0, stop=None, step=1):
    if isinstance(seq, str):
        seq = sequence_positions(seq, start, stop, step)
    if target is None:
        target = source
    for s, i_source in seq:
        if i_target is None:
            i_target = i_source
        yield transform(s, source, target, i_source, i_target)


def sequence_info(seq_name, start=0, stop=None, step=1):
    seq = list(sequence_positions(seq_name, start=start, stop=stop, step=step))
    # lo = np.full((3, 1), np.inf)
    # hi = np.full((3, 1), -np.inf)
    # vel = np.full((3, 1), np.nan)
    # prev_pos = np.full((3, 1), np.nan)
    # vel = []
    pos = []
    stamps = []
    for pose, t in zip(transforms(seq, 'laser'), imu_stamps(seq)):
        p = np.asarray(pose[0:3, 3:])
        # lo = np.minimum(lo, pos)
        # hi = np.maximum(hi, pos)
        pos.append(p)
        stamps.append(t)
    pos = np.concatenate(pos, axis=1)
    ds = np.linalg.norm(pos[:, 1:] - pos[:, 0:-1], axis=0)
    # stamps = np.concatenate(stamps)
    # stamps = np.stack(stamps)
    # dt = stamps[1:] - stamps[0:-1]
    dt = np.array([(t1 - t0).total_seconds() for t0, t1 in zip(stamps[0:-1], stamps[1:])])
    # dt = np.array(dt)
    speed = ds / dt

    # info = {'name': seq_name,
    #         'length': len(seq),
    #         'laser': {'low': lo.flatten().tolist(),
    #                   'high': hi.flatten().tolist(),
    #                   'extents': (hi - lo).flatten().tolist()}}

    info = {'name': seq_name,
            'length': len(seq),
            'position': {'min': pos.min(axis=1).flatten().tolist(),
                         'max': pos.max(axis=1).flatten().tolist(),
                         'range': (pos.max(axis=1) - pos.min(axis=1)).flatten().tolist()},
            'speed': {'min': speed.min(), 'max': speed.max(), 'average': speed.mean()}}
    return info


def print_info():
    # for category in 'city', 'residential', 'road':
    import pandas as pd
    records = []
    for category in 'residential',:
        for seq_name in sequences[category]:
            info = sequence_info(seq_name)
            records.append([category, seq_name, info['length']])
            print(category, seq_name,
                  '[' + ', '.join('%.1f' % x for x in info['position']['range']) + ']',
                  'speed:', *('%s: %.1f m/s' % (k, v) for k, v in info['speed'].items()))
    print(pd.DataFrame(records))


def _test_module_doc():
    import doctest
    doctest.testmod()


if __name__ == '__main__':
    # _test_module_doc()
    print_info()
