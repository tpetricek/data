from __future__ import absolute_import, division, print_function
import csv
import cv2
import numpy as np
import os
import requests
import sys

if sys.version_info[0] == 2:
    from urlparse import urlparse, urlunparse
    range = xrange
else:
    from urllib.parse import urlparse, urlunparse


def read_tuples(path, delimiter=' '):
    url = urlparse(path)
    parts = list(url)
    dir = os.path.dirname(url.path)
    if not url.scheme:
        # Parse data from a local file.
        with open(path, 'rb') as fobj:
            reader = csv.reader(fobj, delimiter=delimiter)
            ret = [[os.path.join(dir, f) for f in files] for files in reader]
            return ret
    # Parse data from URL.
    resp = requests.get(url.geturl())
    resp.raise_for_status()
    reader = csv.reader(resp.text.splitlines(), delimiter=delimiter)
    ret = [[urlunparse(parts[0:2] + [os.path.join(dir, f)] + parts[3:]) for f in files] for files in reader]
    return ret


def read_tuple(paths):
    ret = []
    for p in paths:
        url = urlparse(p)
        if not url.scheme:
            im = cv2.imread(p, -1)
        else:
            resp = requests.get(url.geturl())
            resp.raise_for_status()
            im = cv2.imdecode(np.fromstring(resp.content, dtype=np.uint8), -1)
        if not im.size:
            raise RuntimeError('Error while reading {}.'.format(p))
        ret.append(im)
    return ret


class Data:
    def __init__(self, path, delimiter=' '):
        self.tuples = read_tuples(path, delimiter=delimiter)

    def __len__(self):
        return len(self.tuples)

    def __getitem__(self, i):
        return read_tuple(self.tuples[i])

    def __iter__(self):
        for t in self.tuples:
            yield read_tuple(t)


class Repeated:
    def __init__(self, d, n=1, shuffle=False, rng=None):
        """
        :param d: Data object, modified on shuffle.
        :param n: Number of repeats
        :param shuffle: Whether to shuffle prior every repeat.
        :param rng: Random number generator.
        """
        self.data = d
        self.n = n
        self.shuffle = shuffle
        self.rng = rng

    def __iter__(self):
        for i in range(self.n):
            if self.shuffle:
                if self.rng:
                    self.rng.shuffle(self.data.tuples)
                else:
                    np.random.shuffle(self.data.tuples)
            for images in self.data:
                yield images
