from __future__ import absolute_import, division, print_function
import cv2
import numpy as np
import os


def with_name_suffix(path, suffix):
    prefix, ext = os.path.splitext(path)
    return prefix + suffix + ext


def imreadf(path, prec=1.0, centers=True):
    # TODO: Higher range with more than 2 files.
    assert path[-4:].lower() == '.png'
    r = cv2.imread(with_name_suffix(path, '-0'), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH)
    d = cv2.imread(with_name_suffix(path, '-1'), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH)
    assert d.dtype == np.uint16 and r.dtype == np.uint16
    m = 2**16  # uint16 values
    img = m * d.astype(np.float64) + r.astype(np.float64)
    img *= prec
    if centers:
        img[img > 0] += prec / 2
    return img


def imwritef(img, path, prec=1.0):
    # TODO: Higher range with more than 2 files.
    assert (img >= 0).all()
    img = img.astype(np.float64) / prec
    assert (img < 2**32).all()
    m = 2**16  # uint16 values
    d = np.floor(img / m).astype(np.uint16)
    r = np.floor(np.mod(img, m)).astype(np.uint16)
    assert path[-4:].lower() == '.png'
    cv2.imwrite(with_name_suffix(path, '-0'), r)
    cv2.imwrite(with_name_suffix(path, '-1'), d)
