from __future__ import absolute_import, division, print_function
import cv2
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image, ImagePalette


def bgr2rgb(x):
    return cv2.cvtColor(x, cv2.COLOR_BGR2RGB)


def rgb2bgr(x):
    return cv2.cvtColor(x, cv2.COLOR_RGB2BGR)


def write_indexed_image(fp, x, palette, n):
    x = Image.fromarray(x)
    x = x.convert('P')
    cmap = plt.cm.get_cmap(palette)(np.linspace(0.0, 1.0, n))
    # Remove alpha channel and convert to [0, 255].
    cmap = (cmap[:, 0:3] * 255).astype(np.uint8)
    palette = cmap.flatten().tolist()
    # palette = cmap.T.flatten().tolist()
    x.putpalette(palette)
    x.save(fp)


def read_indexed_image(fp):
    x = np.asarray(Image.open(fp))
    return x
