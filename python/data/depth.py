#!/usr/bin/env python
"""Depth conversion and IO.

Assumes floating-point values are meters, uint16 are mm, if not specified otherwise.

Reads always to floating-point meters. If something else is needed, use imread directly.

Invalid values:
uint16 mm:  0
uint16 inv: 256**2 - 1 = np.iinfo(np.uint16).max
float:      NaN
"""
from __future__ import absolute_import, division, print_function
from ast import literal_eval
import cv2 as cv
from enum import Enum
import numpy as np
from PIL import Image, PngImagePlugin


def meters(x):
    """Converts depth to float [m]."""
    assert isinstance(x, np.ndarray)
    if x.dtype in (np.float32, np.float64):
        return x
    if x.dtype == np.uint16:
        out = (x.astype(np.float32) + .5) / 1000.
        invalid = (x == _DEFAULT_MM_NAN)
        out[invalid] = np.nan
        return out


def milimeters(x):
    """Converts depth to uint16 [mm]."""
    assert isinstance(x, np.ndarray)
    if x.dtype == np.uint16:
        return x
    if x.dtype in (np.float32, np.float64):
        out = (1000. * x).astype(np.uint16)
        invalid = np.isnan(x) | (x < _DEFAULT_MM_MIN) | (x >= np.iinfo(np.uint16).max + 1)
        out[invalid] = _DEFAULT_MM_NAN
    return out


def replace(x, y, map=None):
    """Maps key values in x to map[key] values in y."""
    assert isinstance(x, np.ndarray)
    assert isinstance(y, np.ndarray)
    if isinstance(map, dict):
        for i, o in map.items():
            if np.isnan(i):
                y[np.isnan(x)] = o
            else:
                y[x == i] = o
    return y


def inv(x, a=1.0, b=0.0, map=None):
    """Inverse with scale and offset, y = a / (x + b)."""
    assert isinstance(x, np.ndarray)
    x = meters(x)
    try:
        y = a / (x + b)
    except ZeroDivisionError as ex:
        y = type(x)('inf')
    y = replace(x, y, map=map)
    return y


def inv_inv(x, a=1.0, b=0.0, map=None):
    """Inverse of inv, y = a / x - b."""
    assert isinstance(x, np.ndarray)
    assert x.dtype in (np.float32, np.float64)
    try:
        y = a / x - b
    except ZeroDivisionError as ex:
        y = type(x)('inf') - b
    y = replace(x, y, map=map)
    return y


def inv_km(x, map=None):
    assert isinstance(x, np.ndarray)
    x = meters(x)
    return inv(x, a=1000., map=map)


def inv_km_to_meters(x, map=None):
    assert isinstance(x, np.ndarray)
    assert x.dtype in (np.float32, np.float64)
    return inv(x, 1000., map=map)


def _inv_scale_and_offset(anchors):
    assert len(anchors) == 2
    x = []
    y = []
    for xi, yi in anchors.items():
        x.append(xi)
        y.append(yi)
    b = (x[1]*y[1] - x[0]*y[0]) / (y[0] - y[1])
    a = x[0] * y[0] + y[0] * b
    return a, b


class IOMode(Enum):
    AUTO = 'auto'  # Decides based on depth array type on write, uses metadata on read.
    MM = 'mm'      # Directly to 16-bit PNG as [mm].
    INV = 'inv'    # Inverse depth with scale and offset, 16-bit PNG with metadata.


def write_depth(x, path, mode=IOMode.AUTO, anchors=None):
    assert isinstance(x, np.ndarray)

    if mode == IOMode.AUTO:
        if x.dtype in (np.uint8, np.uint16):
            mode = IOMode.MM
        else:
            mode = IOMode.INV

    if mode == IOMode.MM:
        if x.dtype in (np.float32, np.float64):
            x = (1000. * x).astype(np.uint16)
        assert x.dtype == np.uint16
        cv.imwrite(path, x)
        return

    if mode == IOMode.INV:
        if x.dtype == np.uint16:
            x = meters(x)
        assert x.dtype in (np.float32, np.float64)
        if anchors is not None:
            a, b = _inv_scale_and_offset(anchors)
        else:
            a, b = _DEFAULT_INV_SCALE_AND_OFFSET
        invalid = ~valid(x)
        x = inv(x, a=a, b=b)
        # Due inv being a decreasing function, we have to get ceiling instead
        # of floor for quantization, so that e.g. values lower than minimum
        # are mapped to invalid.
        x = np.ceil(x)
        invalid = invalid | (x >= _DEFAULT_INV_NAN)
        x = x.astype(np.uint16)
        x[invalid] = _DEFAULT_INV_NAN
        assert x.dtype == np.uint16
        img = Image.fromarray(x)
        # img.info['inv_scale_and_offset'] = str((a, b))
        # cv.imwrite(path, x)
        info = PngImagePlugin.PngInfo()
        info.add_text('inv_scale_and_offset', str((a, b)))
        # img.save(path, pnginfo={'inv_scale_and_offset': str((a, b))})
        img.save(path, pnginfo=info)
        return


def read_depth(path, mode=IOMode.AUTO, anchors=None):
    img = Image.open(path)
    x = np.array(img)

    if mode == IOMode.MM:
        return meters(x)

    has_scale_and_offset = ('inv_scale_and_offset' in img.info)
    if mode == IOMode.AUTO and not has_scale_and_offset:
        return meters(x)

    assert mode == IOMode.INV or has_scale_and_offset
    if has_scale_and_offset:
        a, b = literal_eval(img.info['inv_scale_and_offset'])
    elif anchors is not None:
        a, b = _inv_scale_and_offset(anchors)
    else:
        a, b = _DEFAULT_INV_SCALE_AND_OFFSET
    invalid = (x == _DEFAULT_INV_NAN)
    # Due inv being a decreasing function, we have to decrease 0.5 instead of
    # of increasing to get to the center of corresponding ranges.
    x = x.astype(np.float) - 0.5
    x[x < 0.0] = 0.0
    x[invalid] = np.nan
    x = inv_inv(x, a, b)
    return x


def valid(x):
    assert isinstance(x, np.ndarray)
    if x.dtype == np.uint16:
        return x == _DEFAULT_MM_NAN
    assert x.dtype in (np.float32, np.float64)
    return ~np.isnan(x) & (x > 0.)


_DEFAULT_MM_NAN = 0
_DEFAULT_MM_MIN = 0.001

_DEFAULT_INV_NAN = np.iinfo(np.uint16).max
_DEFAULT_INV_MIN = 0.001
_DEFAULT_INV_ANCHORS = {_DEFAULT_INV_MIN: np.iinfo(np.uint16).max - 1,
                        2 * _DEFAULT_INV_MIN: np.iinfo(np.uint16).max - 2}
_DEFAULT_INV_SCALE_AND_OFFSET = _inv_scale_and_offset(_DEFAULT_INV_ANCHORS)


def main():
    for x0 in [1e-4, 1e-3, 1e-2,
               1e-1, 2e-1, 5e-1,
               1., 2., 5.,
               10., 20., 50.,
               1e2, 2e2, 5e2,
               1e3, 2e3, 5e3,
               1e4, 2e4, 5e4,
               1e5, 2e5, 5e5,
               1e6, 2e6, 5e6,
               1e7, 2e7, 5e7]:
        x0 = np.array([x0])
        i0 = inv(x0, *_DEFAULT_INV_SCALE_AND_OFFSET)
        for i in [i0, i0 - 1]:
            x = inv_inv(i, *_DEFAULT_INV_SCALE_AND_OFFSET)
            print('%05f: inv: %9.6f' % (i, x))


if __name__ == '__main__':
    main()
