from collections.abc import Sequence
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np


# https://matplotlib.org/examples/pylab_examples/image_slices_viewer.html
class Index:
    def __init__(self, ax, seq, ind=0):
        self.ax = ax
        self.seq = seq
        self.ind = ind
        self.im = None
        self.update()

    def onscroll(self, event):
        # print("%s %s" % (event.button, event.step))
        if event.button == 'up':
            self.ind = np.clip(self.ind + 1, 0, len(self.seq) - 1)
        else:
            self.ind = np.clip(self.ind - 1, 0, len(self.seq) - 1)
        self.update()

    def update(self):
        if self.im is None:
            cmap = 'gray' if self.seq[self.ind].ndim == 2 else None
            self.im = self.ax.imshow(self.seq[self.ind], cmap=cmap)
        self.im.set_data(self.seq[self.ind])
        self.ax.set_title(str(self.ind))
        self.im.axes.figure.canvas.draw()


def show_images(images):
    """Show images.

    :param iterator images: Images to show.
    >>> from data.kitti import images, sequences
    >>> show_images(images(sequences['city'][0], camera=2))
    """
    images = list(images)
    fig, ax = plt.subplots(1, 1)
    index = Index(ax, images)
    fig.canvas.mpl_connect('scroll_event', index.onscroll)
    plt.show()


def show_origins(poses):
    """Show origins from poses.

    :param iterator poses: Poses to show.
    >>> from data.kitti import sequences, laser_scan_poses
    >>> show_origins(laser_scan_poses(sequences['city'][1]))
    """
    poses = list(poses)
    x = [p[0, 3] for p in poses]
    y = [p[1, 3] for p in poses]
    z = [p[2, 3] for p in poses]
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(x, y, z)
    plt.show(block=True)


def plot_pose(ax, pose, projection=None):
    pose = np.asarray(pose)
    i, j, k, o = pose[0:3, 0], pose[0:3, 1], pose[0:3, 2], pose[0:3, 3]
    # ax.plot([o[0], o[0] + i[0]], [o[1], o[1] + i[1]], [o[2], o[2] + i[2]], 'r')
    colors = 'rgb'
    if projection == '3d':
        for b, c in zip((i, j, k), colors):
            ax.plot([o[0], o[0] + b[0]], [o[1], o[1] + b[1]], [o[2], o[2] + b[2]], c)
    else:
        for b, c in zip((i, j, k), colors):
            ax.plot([o[0], o[0] + b[0]], [o[1], o[1] + b[1]], c)


def show_poses(poses, projection=None):
    """Show origins from poses.

    :param iterator poses: Poses to show.
    :param str projection: Projection type.
    >>> from data.kitti import sequences, laser_scan_poses
    >>> show_poses(laser_scan_poses(sequences['city'][0]), projection=None)
    """
    fig = plt.figure()
    ax = fig.gca(projection=projection)
    ax.set_aspect('equal')
    plt.xlabel('x')
    plt.ylabel('y')
    for p in poses:
        plot_pose(ax, p, projection=projection)
    plt.show(block=True)


def testdoc():
    import doctest
    doctest.testmod()


if __name__ == '__main__':
    testdoc()
