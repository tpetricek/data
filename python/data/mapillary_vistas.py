from __future__ import absolute_import, division, print_function
import cv2 as cv
from enum import Enum, IntEnum
from glob import glob
import json
import numpy as np
import os
from PIL import Image

# https://github.com/mapillary/mapillary_vistas


class MapillaryVistas(object):

    class Split(Enum):
        train = 'training'
        val = 'validation'
        test = 'testing'

        def __str__(self):
            return self.name

        def dirname(self):
            return self.value

    class Datum(IntEnum):
        rgb = 0
        segmentation = 1
        instances = 2
        panoptic = 3

        def __str__(self):
            return self.name

        def __int__(self):
            return self.value

        def dirname(self):
            return ['images', 'labels', 'instances', 'panoptic'][self.value]

        def ext(self):
            return ['.jpg', '.png', '.png', '.png'][self.value]

    dataset_name = 'mapillary_vistas'
    dataset_dir = dataset_name

    def __init__(self, root,
                 data=(Datum.rgb, Datum.segmentation, Datum.instances, Datum.panoptic),
                 split=Split.train):
        data = list(data)
        for i in range(len(data)):
            if isinstance(data[i], str):
                data[i] = self.Datum[data[i]]
            assert data[i] in self.Datum
        if isinstance(split, str):
            split = self.Split[split]
        assert split in self.Split
        self.root = root
        self.data = data
        self.split = split
        self._formats = [os.path.join(self.root, self.split.dirname(), d.dirname(), '%%s%s' % d.ext())
                         for d in self.Datum]

        with open(os.path.join(self.root, 'config.json'), 'r') as f:
            self.config = json.load(f)

        names = []
        for p in glob(self.path(self.Datum.rgb, '*')):
            name = os.path.splitext(os.path.split(p)[1])[0]
            names.append(name)
        self.names = sorted(names)

    def categories(self):
        return [label['readable'] for label in self.config['labels']]

    def num_segmentation_categories(self):
        return len(self.config['labels'])

    def name(self, i):
        if isinstance(i, str):
            return i
        elif isinstance(i, int):
            return self.names[i]
        raise TypeError('Invalid name type: %s.' % type(i))

    def path(self, datum, i):
        path = self._formats[datum] % self.name(i)
        return path

    def rgb(self, i):
        image = cv.imread(self.path(self.Datum.rgb, i), cv.IMREAD_COLOR)
        image = cv.cvtColor(image, cv.COLOR_BGR2RGB)
        return image

    def segmenation(self, i):
        path = self.path(self.Datum.segmentation, i)
        # image = cv.imread(path, cv.IMREAD_ANYDEPTH)
        image = Image.open(path)
        image = np.array(image)
        return image

    def __len__(self):
        return len(self.names)

    def __getitem__(self, i):
        d = {'name': self.name(i),
             'dataset': self.dataset_name}
        if self.Datum.rgb in self.data:
            d['rgb'] = self.rgb(i)
        if self.Datum.segmentation in self.data:
            d['segmentation'] = self.segmenation(i)
            d['segmentation_categories'] = self.num_segmentation_categories()
        return d
