import numpy as np


def translation(t, d=None, dtype=np.float64):
    """Translation in projective space, (d+1)x(d+1) matrix.
    :param t:
    :param d:
    :param dtype:
    :return:

    >>> translation([-1, -2])[0:2, 2].tolist()
    [[-1.0], [-2.0]]
    >>> translation(2.0, d=3).sum()
    10.0
    """
    if d is None:
        d = len(t)
    assert d is not None
    tf = np.eye(d + 1, dtype=dtype)
    tf[0:d, d] = t
    return tf


def transform_points(trans, x):
    assert 3 <= trans.shape[0] <= 4
    assert trans.shape[1] == 4
    assert x.ndim == 2
    assert x.shape[0] == 3
    return np.matmul(trans[0:3, 0:3], x) + trans[0:3, 3:4]


def inverse(tf):
    return np.linalg.inv(tf)


def relative_transform(source, target):
    return np.linalg.solve(source.T, target.T).T


def camera_matrix(fov, size):
    """Camera matrix K.

    :param fov: Field of view in radians.
    :param size: Image size, (height, width).
    :return: 3-by-3 camera matrix.
    """
    if np.isscalar(size):
        h, w = size, size
    else:
        h, w = size
    if np.isscalar(fov):
        fx = max([h, w]) / np.tan(fov / 2) / 2
        fy = fx
    else:
        fx = w / np.tan(fov[0] / 2) / 2
        fy = h / np.tan(fov[1] / 2) / 2

    return np.array([[fx,  0, w / 2 - 0.5],
                     [ 0, fy, h / 2 - 0.5],
                     [ 0 , 0,           1]])


def scale_camera_matrix(K, s):
    K = K.copy()
    if s == 1.0:
        return K
    K[0, 0] *= s
    K[1, 1] *= s
    K[0, 2] = (K[0, 2] + 0.5) * s - 0.5
    K[1, 2] = (K[1, 2] + 0.5) * s - 0.5
    return K


def camera_fov(K, size):
    """Camera field of view.

    :param K: 3-by-3 camera matrix
    :param size: Image size, (height, width)
    :return: Field of view in radians, (fov_x, fov_y).
    """
    assert np.allclose(K[2, :], [0, 0, 1])
    fov_x = 2 * np.arctan((size[1] / 2) / K[0, 0])
    fov_y = 2 * np.arctan((size[0] / 2) / K[1, 1])
    return fov_x, fov_y
