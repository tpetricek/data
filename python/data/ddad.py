from __future__ import absolute_import, division, print_function
from .depth import write_depth, read_depth, valid
from .inpainting import distance_to_valid, inpaint
import cv2 as cv
from dgp.datasets import base_dataset
from dgp.datasets import SynchronizedSceneDataset
import numpy as np
import os
import sys
from timeit import default_timer as timer
import tqdm

# Avoid heavy multiprocessing in loading the dataset.
# base_dataset.Pool
base_dataset.cpu_count = lambda: 1

LIDARS = ('lidar',)
CAMERAS = tuple('CAMERA_%02i' % i for i in (1, 5, 6, 7, 8, 9))


def get_scene_indices(dataset, scene_idx):
    return [i for i in range(len(dataset)) if dataset.dataset_item_index[i][0] == scene_idx]


def generate_depths(json_path, format=None, split='train', scene_idx=None,
                    dense=False, dense_format=None,
                    valid_depth_dist=False, valid_depth_dist_format=None):
    """Generate and save depth images.

    :param path: Path to ddad.json file.
    :param format: Output depth image format.
    :param dense: Output dense depth too?
    :param dense_format: Output dense depth image format.
    """
    dir = os.path.dirname(json_path)
    if format is None:
        format = os.path.join(dir, '{sequence:06d}', 'depth.gen', '{camera:s}', '{timestamp:d}.png')
    if dense and dense_format is None:
        dense_format = os.path.join(dir, '{sequence:06d}', 'dense_depth.gen', '{camera:s}', '{timestamp:d}.png')
    if valid_depth_dist and valid_depth_dist_format is None:
        valid_depth_dist_format = os.path.join(dir, '{sequence:06d}', 'valid_depth_dist.gen', '{camera:s}', '{timestamp:d}.png')

    # TODO: All splits
    dataset = SynchronizedSceneDataset(
        json_path,
        datum_names=LIDARS + CAMERAS,
        generate_depth_from_datum=LIDARS[0],
        split=split
    )

    if scene_idx is None:
        indices = range(dataset)
    else:
        indices = get_scene_indices(dataset, scene_idx)

    radius = 255
    rmse = []
    mae = []
    for index in tqdm.tqdm(indices):
        scene_idx, sample_idx, datum_indices = dataset.dataset_item_index[index]
        sample = dataset[index]

        for camera in sample[len(LIDARS):len(LIDARS) + len(CAMERAS)]:
            depth = camera['depth']
            # depth[depth == 0.0] = np.nan
            mask = valid(depth)
            depth[~mask] = np.nan

            depth_path = format.format(sequence=scene_idx,
                                       camera=camera['datum_name'],
                                       timestamp=camera['timestamp'])
            os.makedirs(os.path.dirname(depth_path), exist_ok=True)
            write_depth(depth, depth_path)

            depth_read = read_depth(depth_path)
            error = depth[mask] - depth_read[mask]
            rmse.append(np.sqrt((error**2).mean()))
            mae.append(np.abs(error).mean())
            # print('%s (MAE = %.3g m, RMSE = %.3g).' % (depth_path, mae, rmse))
            # tqdm.tqdm.write('%s (MAE = %.3g m, RMSE = %.3g).' % (depth_path, mae, rmse))

            if dense:
                t = timer()
                dense_depth = inpaint(depth, ~mask, 'nearest')
                # dense_depth = inpaint(depth, ~mask, 'navier_stokes', radius=radius)
                # dense_depth = inpaint(depth, ~mask, 'telea', radius=radius)
                # print('Inpainting: %.3f s' % (timer() - t))
                dense_path = dense_format.format(sequence=scene_idx,
                                                 camera=camera['datum_name'],
                                                 timestamp=camera['timestamp'])
                os.makedirs(os.path.dirname(dense_path), exist_ok=True)
                write_depth(dense_depth, dense_path)

                # dense_read = read_depth(dense_path)
                # error = dense_depth[mask] - dense_read[mask]
                # rmse = np.sqrt((error ** 2).mean())
                # mae = np.abs(error).mean()
                # print('%s (MAE = %.3g m, RMSE = %.3g).' % (dense_path, mae, rmse))
                # tqdm.tqdm.write('%s (MAE = %.3g m, RMSE = %.3g).' % (dense_path, mae, rmse))

            if valid_depth_dist:
                valid_dist = distance_to_valid(~mask, np.uint8)
                valid_dist_path = valid_depth_dist_format.format(sequence=scene_idx,
                                                                 camera=camera['datum_name'],
                                                                 timestamp=camera['timestamp'])
                os.makedirs(os.path.dirname(valid_dist_path), exist_ok=True)
                cv.imwrite(valid_dist_path, valid_dist)

    mae = np.array(mae)
    rmse = np.array(rmse)
    print('MAE: %s' % mae)
    print('RMSE: %s' % rmse)
    print('Max. MAE: %.6f' % mae.max())
    print('Max. RMSE: %.6f' % rmse.max())


def main():
    argv = sys.argv[1:]
    argc = len(argv)
    if argc <= 0:
        return

    verb = argv[0]
    if verb == 'generate_depths':
        assert argc >= 2
        json_path = argv[1]
        split = argv[2] if argc >= 3 else 'train'
        scene_idx = int(argv[3]) if argc >= 4 else None
        generate_depths(json_path, split=split, scene_idx=scene_idx,
                        dense=True, valid_depth_dist=True)


if __name__ == '__main__':
    main()
