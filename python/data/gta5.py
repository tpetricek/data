from __future__ import absolute_import, division, print_function
import cv2
from . import data_dir, imreadf, imwritef
from enum import IntEnum
from functools import lru_cache
from glob import glob
import json
import numpy as np
import os
import tifffile as tif


def path(*parts):
    parts = list(parts)
    for i, p in enumerate(parts):
        if isinstance(p, int):
            parts[i] = '%i' % p
    return os.path.join(data_dir, 'gta5', *parts)


# TODO: Prune incomplete cameras with incomplete data?
@lru_cache()
def json_paths(seq, camera):
    return sorted(glob(path(seq, camera, '*.json')))


def positions(seq, camera=None, only_complete=False):
    if camera is None:
        pos = set()
        for i, camera in enumerate(cameras):
            if only_complete:
                if i == 0:
                    pos = set(positions(seq, camera))
                else:
                    pos &= set(positions(seq, camera))
            else:
                pos |= set(positions(seq, camera))
        return sorted(pos)
    return sorted(int(os.path.splitext(os.path.basename(p))[0]) for p in json_paths(seq, camera))


def sequence_length(seq, camera):
    # return len(json_paths(seq, camera))
    return len(positions(seq, camera))


def json_path(seq, camera, i):
    return path(seq, camera, '%06i.json' % i)


def rgb_path(seq, camera, i):
    # return path(seq, camera, camera_info(seq, camera, i)['imagepath'] + '.jpg')
    return path(seq, camera, '%06i.jpg' % i)


def depth_path(seq, camera, i):
    # return path(seq, camera, camera_info(seq, camera, i)['imagepath'] + '-depth.png')
    return path(seq, camera, '%06i-depth.png' % i)
    # return path(seq, camera, '%06i-depth.tiff' % i)


def stencil_path(seq, camera, i):
    # return path(seq, camera, camera_info(seq, camera, i)['imagepath'] + '-stencil.png')
    return path(seq, camera, '%06i-stencil.png' % i)


def local_cloud_path(seq, camera, i):
    return path(seq, 'local_cloud_%s' % camera, '%06i.csv' % i)


def camera_pose_path(seq, camera, i):
    return path(seq, 'camera_pose_%s' % camera, '%06i.csv' % i)


def global_cloud_path(seq, camera, i):
    return path(seq, 'global_cloud_%s' % camera, '%06i.csv' % i)


@lru_cache()
def camera_info(seq, camera, i):
    # with open(json_paths(seq, camera)[i], 'r', encoding='utf-8') as f:
    with open(json_path(seq, camera, i), 'r', encoding='utf-8') as f:
        return json.load(f)


def bgr(seq, camera, i):
    return cv2.imread(rgb_path(seq, camera, i), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH)


def rgb(seq, camera, i):
    # return bgr(seq, camera, i)[:, :, [2, 1, 0]]
    return cv2.cvtColor(bgr(seq, camera, i), cv2.COLOR_BGR2RGB)


def depth(seq, camera, i, units='meters'):
    assert units in ('ndc', 'meters')
    if units == 'meters':
        try:
            # return imreadf(depth_path(seq, camera, i), prec=0.001)
            return imreadf(path(seq, camera, '%06i-depth.png' % i), prec=0.001)
        except Exception as ex:
            print(ex)
    # Fallback to NDC in tiff.
    # return cv2.imread(depth_path(seq, camera, i), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH)
    d = tif.imread(path(seq, camera, '%06i-depth.tiff' % i))
    if units == 'ndc':
        return d
    return depth_to_meters(d, camera_info(seq, camera, i))


def stencil(seq, camera, i):
    return cv2.imread(stencil_path(seq, camera, i), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH)


def depth_to_meters(depth, info):
    if depth.dtype in (np.uint8, np.uint16, np.uint32):
        depth = depth / np.iinfo(depth.dtype).max
    proj = np.array(info['proj_matrix'])
    meters = p2e(np.linalg.solve(proj[2:][:, 2:], e2p(depth.reshape((1, -1)))))
    return -meters.reshape(depth.shape)


def e2p(x, axis=0):
    sz = list(x.shape)
    sz[axis] = 1
    return np.concatenate((x, np.ones(sz)), axis=axis)


def p2e(x, axis=0):
    assert axis == 0
    return x[:-1, :] / x[-1, :]


def segmentation(stencil):
    return np.bitwise_and(stencil, 7)


def artificial_light_source_mask(stencil):
    return np.bitwise_and(stencil, 8).astype(np.bool)


def main_characters_car_mask(stencil):
    return np.bitwise_and(stencil, 16).astype(np.bool)


def main_player_character_mask(stencil):
    return np.bitwise_and(stencil, 64).astype(np.bool)


def full_frame_spec(frame):
    if frame == 'world':
        return frame
    elif isinstance(frame, str):
        return (frame, 0, 'view')
    elif isinstance(frame, (list, tuple)):
        # assert frame != 'world'
        if len(frame) == 1:
            return frame + (0, 'view')
        elif len(frame) == 2:
            return frame + ('view',)
        assert len(frame) == 3
        return frame


def transform(seq, *args):
    """Transform between arbitrary frames.

    tf_rl = transform(seq, target)
    tf_rl = transform(seq, source, target)

    :param seq:
    :param source:
    :param target:
    :return:
    """
    assert 1 <= len(args) <= 2
    if len(args) == 1:
        source, target = 'world', args[0]
    elif len(args) == 2:
        source, target = args
    if isinstance(target, np.ndarray):
        return target @ np.linalg.inv(transform(seq, 'world', source))
    elif source == target:
        return np.eye(4)
    elif source == 'world':
        # Assume target is a camera.
        info = camera_info(seq, target[0], target[1])
        if len(target) == 2 or target[2] == 'camera':
            return view_to_camera @ np.array(info['view_matrix'])
        elif target[2] == 'view':
            return np.array(info['view_matrix'])
        elif target[2] == 'ndc':
            return np.array(info['proj_matrix']) @ np.array(info['view_matrix'])
    elif target == 'world':
        return np.linalg.inv(transform(seq, target, source))
    else:
        return transform(seq, 'world', target) @ transform(seq, source, 'world')


def depth_cloud(seq, camera, i, target='camera', step=1, grid=False):
    assert isinstance(target, (list, tuple, np.ndarray)) or target in ('camera', 'view', 'world')
    z = depth(seq, camera, i, units='meters')
    y, x = np.meshgrid(np.arange(0, z.shape[0], step),
                       np.arange(0, z.shape[1], step),
                       indexing='ij')
    h, w = y.shape
    if step > 1:
        z = z[::step, ::step]
    if z.dtype in (np.uint8, np.uint16, np.uint32):
        z = z / np.iinfo(z.dtype).max
    K, R, t, _ = projection_matrix(seq, camera, i, factorized=True)
    # To 3xN matrix with points in columns, at unit z.
    x = np.linalg.solve(K, e2p(np.stack((x.flatten(), y.flatten()), axis=0)))
    # Multiply by depth.
    x *= z.reshape((1, -1))
    # For camera target frame, do nothing, otherwise transform as neeeded.
    if isinstance(target, (list, tuple)):
        # A general transform...
        tf = transform(seq, (camera, i, 'camera'), target)
        x = p2e(tf @ e2p(x))
    elif isinstance(target, np.ndarray):
        tf = target @ np.block([[R.T, -R.T @ t], [0., 0., 0., 1.]])
        x = p2e(tf @ e2p(x))
    elif target.lower() == 'view':
        x = camera_to_view[0:3, 0:3] @ x
    elif target.lower() == 'ndc':
        proj = camera_info(seq, camera, i)['proj_matrix']
        x = p2e(proj @ camera_to_view @ e2p(x))
    elif target.lower() == 'world':
        x = R.T @ (x - t)
    else:
        assert target.lower() == 'camera'
    if grid:
        x = x.T.reshape((h, w, 3))
    return x


def view_matrix(seq, camera, i):
    return np.array(camera_info(seq, camera, i)['view_matrix'])


def camera_pose(seq, camera, i):
    return np.linalg.inv(view_matrix(seq, camera, i))


def ndc_to_dev(size):
    """NDC to device-specific coordinates.
    :param size: (height, width)
    :return:
    """
    h, w = size
    return np.array([[w / 2,      0, w / 2 - 0.5],
                     [    0, -h / 2, h / 2 - 0.5],
                     [    0,      0,           1]])


def projection_matrix(seq, camera, i, size=None, factorized=False):
    """Camera projection P = K*[R t].
    :param seq:
    :param camera:
    :param i:
    :param size: (height, width)
    :param factorized: (K, R, t, C) if True, or P.
    :return:
    """
    info = camera_info(seq, camera, i)
    if size is None:
        size = info['height'], info['width']
    dev = ndc_to_dev(size)
    rmz = np.array([[1, 0, 0, 0],
                    [0, 1, 0, 0],
                    [0, 0, 0, 1]], dtype=np.float64)
    proj = np.array(info['proj_matrix'])
    view = np.array(info['view_matrix'])
    P = dev @ rmz @ proj @ view
    if factorized:
        # K, R, t = cv2.decomposeProjectionMatrix(P)[0:3]
        # t = p2e(t)
        # C = -R.T @ t
        K, R, C = cv2.decomposeProjectionMatrix(P)[0:3]
        C = p2e(C)
        t = -R @ C
        return K, R, t, C
    return P


class Label(IntEnum):
    BACKGROUND = 0
    PERSON = 1
    VEHICLE = 2
    VEGETATION = 3
    GRASSY_BG = 4
    SKY = 7


camera_to_view = np.diag([1., -1., -1., 1.])
view_to_camera = np.diag([1., -1., -1., 1.])
sequences = [
    'offroad-7',
    'offroad-8',
    'offroad-9',
    'offroad-14',
    'offroad-15',
    'offroad-17'
]
# cameras = ['0', '1', '2', '3', '4', '5']
# stereo = ['1', '4']
cameras = [0, 1, 2, 3, 4, 5]
stereo = [1, 4]
stereo_height = 1.65    # Height above ground (from KITTI paper)
stereo_baseline = 0.54  # Baseline (from KITTI paper)
