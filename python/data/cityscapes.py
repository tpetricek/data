from __future__ import absolute_import, division, print_function
import cv2 as cv
from enum import Enum, IntEnum
from glob import glob
import json
import numpy as np
import os
from PIL import Image
# from torchvision.datasets.cityscapes import Cityscapes
from .common import SEP

# https://github.com/mcordts/cityscapesScripts


def parse_path(path):
    # {root}/{type}{video}/{split}/{city}/{city}_{seq:0>6}_{frame:0>6}_{type}{ext}
    dir, name = os.path.split(path)
    split = os.path.split(os.path.split(dir)[0])[1]
    split = CityScapes.Split[split]

    i_seq = name.index('_') + 1
    i_frame = name.index('_', i_seq) + 1
    i_datum = name.index('_', i_frame) + 1

    city = name[:i_seq - 1]
    seq = int(name[i_seq:i_frame - 1])
    frame = int(name[i_frame:i_datum - 1])
    datum = name[i_datum:]
    if datum != 'leftImg8bit':
        ground_truth, datum = datum.split('_')
        ground_truth = CityScapes.GroundTruth.from_label(ground_truth)
    else:
        ground_truth = None
    datum = CityScapes.Datum.from_label(datum)

    parts = {'split': split,
             'city': city,
             'seq': seq,
             'frame': frame,
             'datum': datum}
    if ground_truth is not None:
        parts['ground_truth'] = ground_truth

    return parts


class CityScapes(object):

    class Split(Enum):
        train = 'train'
        train_extra = 'train_extra'
        val = 'val'
        test = 'test'

        def __str__(self):
            return self.name

        def dirname(self):
            return self.value

    class GroundTruth(Enum):
        coarse = 0
        fine = 1

        def label(self):
            return ['gtCoarse', 'gtFine'][self.value]

        @staticmethod
        def from_label(label):
            for gt in __class__:
                if gt.label() == label:
                    return gt

    class Datum(IntEnum):
        rgb = 0
        segmentation = 1
        instances = 2

        def __str__(self):
            return self.name

        def __int__(self):
            return self.value

        # def dirname(self):
        #     return ['images', 'labels', 'instances'][self.value]

        def label(self):
            return ['leftImg8bit', 'labelIds', 'instanceIds'][self.value]

        @staticmethod
        def from_label(label):
            for gt in __class__:
                if gt.label() == label:
                    return gt

        def ext(self):
            return ['.jpg', '.png', '.png'][self.value]

    dataset_name = 'cityscapes'
    dataset_dir = dataset_name

    def __init__(self, root,
                 data=(Datum.rgb, Datum.segmentation, Datum.instances),
                 split=Split.train,
                 ground_truth=GroundTruth.fine):

        data = list(data)
        for i in range(len(data)):
            if isinstance(data[i], str):
                data[i] = self.Datum[data[i]]
            assert data[i] in self.Datum
        if isinstance(split, str):
            split = self.Split[split]
        assert split in self.Split
        if isinstance(ground_truth, str):
            ground_truth = CityScapes.GroundTruth[ground_truth]
        assert ground_truth in CityScapes.GroundTruth

        self.root = root
        self.data = data
        self.split = split
        self.ground_truth = ground_truth

        self._formats = [os.path.join(self.root, self.split.dirname(), d.dirname(), '%%s%s' % d.ext())
                         for d in self.Datum]

        names = []
        path = os.path.join(self.root, ground_truth.label(), split.name, '*',
                            '*%s_%s.png' % (ground_truth.label(), CityScapes.Datum.segmentation.label()))
        for p in glob(path):
            name = os.path.splitext(os.path.split(p)[1])[0]
            names.append(name)
        self.names = sorted(names)

    @staticmethod
    def from_desc(desc, root):
        ds, ground_truth, split = desc.split(SEP)
        if ds != CityScapes.dataset_name:
            return None
        ground_truth = CityScapes.GroundTruth[ground_truth]
        split = CityScapes.Split[split]
        return CityScapes(root, split=split, ground_truth=ground_truth)

    def categories(self):
        return [label['readable'] for label in self.config['labels']]

    def num_segmentation_categories(self):
        return len(self.config['labels'])

    def name(self, i):
        if isinstance(i, str):
            return i
        elif isinstance(i, int):
            return self.names[i]
        raise TypeError('Invalid name type: %s.' % type(i))

    def path(self, datum, i):
        path = self._formats[datum] % self.name(i)
        return path

    def rgb(self, i):
        image = cv.imread(self.path(self.Datum.rgb, i), cv.IMREAD_COLOR)
        image = cv.cvtColor(image, cv.COLOR_BGR2RGB)
        return image

    def segmenation(self, i):
        path = self.path(self.Datum.segmentation, i)
        # image = cv.imread(path, cv.IMREAD_ANYDEPTH)
        image = Image.open(path)
        image = np.array(image)
        return image

    def __len__(self):
        return len(self.names)

    def __getitem__(self, i):
        d = {'name': self.name(i),
             'dataset': self.dataset_name}
        if self.Datum.rgb in self.data:
            d['rgb'] = self.rgb(i)
        if self.Datum.segmentation in self.data:
            d['segmentation'] = self.segmenation(i)
            d['segmentation_categories'] = self.num_segmentation_categories()
        return d
