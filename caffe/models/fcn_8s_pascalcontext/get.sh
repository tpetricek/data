# FCN-8s Fully Convolutional Semantic Segmentation on PASCAL-Context
# https://gist.github.com/shelhamer/91eece041c19ff8968ee

wget http://dl.caffe.berkeleyvision.org/fcn-8s-pascalcontext.caffemodel
wget https://gist.github.com/shelhamer/91eece041c19ff8968ee/raw/829ca42202f21c884c13953dd0f1d484593f1b27/deploy.prototxt
wget https://gist.github.com/shelhamer/91eece041c19ff8968ee/raw/829ca42202f21c884c13953dd0f1d484593f1b27/solve.py
wget https://gist.github.com/shelhamer/91eece041c19ff8968ee/raw/829ca42202f21c884c13953dd0f1d484593f1b27/solver.prototxt
wget https://gist.github.com/shelhamer/91eece041c19ff8968ee/raw/829ca42202f21c884c13953dd0f1d484593f1b27/train_val.prototxt

