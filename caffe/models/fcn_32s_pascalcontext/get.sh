# FCN-32s Fully Convolutional Semantic Segmentation on PASCAL-Context
# https://gist.github.com/shelhamer/80667189b218ad570e82

wget http://dl.caffe.berkeleyvision.org/fcn-32s-pascalcontext.caffemodel
wget https://gist.github.com/shelhamer/80667189b218ad570e82/raw/077494f215421b3d9383e1b1a3d75377344b1744/deploy.prototxt
wget https://gist.github.com/shelhamer/80667189b218ad570e82/raw/077494f215421b3d9383e1b1a3d75377344b1744/eval.py
wget https://gist.github.com/shelhamer/80667189b218ad570e82/raw/077494f215421b3d9383e1b1a3d75377344b1744/solve.py
wget https://gist.github.com/shelhamer/80667189b218ad570e82/raw/077494f215421b3d9383e1b1a3d75377344b1744/solver.prototxt
wget https://gist.github.com/shelhamer/80667189b218ad570e82/raw/077494f215421b3d9383e1b1a3d75377344b1744/train_val.prototxt

