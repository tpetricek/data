# ILSVRC-2014 model (VGG team) with 16 weight layers
# https://gist.github.com/ksimonyan/211839e770f7b538e2d8

wget http://www.robots.ox.ac.uk/~vgg/software/very_deep/caffe/VGG_ILSVRC_16_layers.caffemodel
wget https://gist.github.com/ksimonyan/211839e770f7b538e2d8/raw/c3ba00e272d9f48594acef1f67e5fd12aff7a806/VGG_ILSVRC_16_layers_deploy.prototxt

# ILSVRC-2014 model (VGG team) with 19 weight layers
# https://gist.github.com/ksimonyan/3785162f95cd2d5fee77

wget http://www.robots.ox.ac.uk/~vgg/software/very_deep/caffe/VGG_ILSVRC_19_layers.caffemodel
wget https://gist.github.com/ksimonyan/3785162f95cd2d5fee77/raw/bb2b4fe0a9bb0669211cf3d0bc949dfdda173e9e/VGG_ILSVRC_19_layers_deploy.prototxt

wget https://github.com/BVLC/caffe/blob/dev/matlab/caffe/matcaffe_demo_vgg_mean_pix.m

echo The models are currently supported by the dev branch of Caffe, but are not yet compatible with master.

