from data.geom import transform_points, inverse
from data.kitti import laser_scans, imu_data, imu_pose, imu_to_laser, sequence_length, sequences, transforms
from data.viz import show_origins, show_poses
import matplotlib.pyplot as plt
import numpy as np
from scipy.sparse import coo_matrix
from voxel_map import VoxelMap

# for seq in sequences['all']:
#     print('%s: %i positions' % (seq, sequence_length(seq)))
# exit(0)

np.set_printoptions(precision=6, suppress=True)
# seq = sequences['city'][0]
# seq = '2011_09_26_drive_0001_sync'
# seq = '2011_09_26_drive_0039_sync'
# seq = '2011_09_26_drive_0046_sync'
# seq = '2011_09_26_drive_0101_sync'  # road, 936 positions
# seq = '2011_09_26_drive_0019_sync'  # residential
seq = '2011_09_26_drive_0027_sync'
print('Sequence %s with %i positions.' % (seq, sequence_length(seq)))
pos = dict(step=sequence_length(seq) // 10)

# for scan in laser_scans(seq, **pos):
#     dist = np.linalg.norm(scan, axis=0)
#     print('max', dist.max(), 'mean', dist.mean())
# exit(0)

show_origins(transforms(seq, 'laser', **pos))
show_poses(transforms(seq, 'laser', **pos))

map = VoxelMap()
map.voxel_size = 0.25
map.free_update = -1.0
map.hit_update = 1.0
map.occupied_threshold = 0.0

for scan, pose in zip(laser_scans(seq, **pos), transforms(seq, 'laser', **pos)):
    points = transform_points(pose, scan)
    sensor_origin = pose[0:3, 3:4]
    map.update_lines(sensor_origin + np.zeros_like(points), points)
    print('%i points added, %i total voxels.' % (points.shape[1], map.size()))

[x, level, occ] = map.get_voxels()
ijk = np.round((x - x.min(axis=1, keepdims=True)) / map.voxel_size).astype(int)
i, j, k = ijk
shape = ijk.max(axis=1) + 1
print('Map size: %s, full voxels: %i' % (shape, np.prod(shape)))

# smat = coo_matrix((occ, (i, j)), shape=shape[0:2])
smat = coo_matrix(((occ >= map.occupied_threshold).astype(float), (i, j)), shape=shape[0:2])
topdown = smat.toarray()
topdown -= topdown.min()
topdown /= topdown.max()
plt.imshow(topdown)
plt.gca().invert_yaxis()
plt.show()
