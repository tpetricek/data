from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import data
import numpy as np
import os
import requests
import unittest


class TestData(unittest.TestCase):

    def _check_type_and_size(self, im, dtype, ndim):
        self.assertEqual(im.dtype, dtype)
        self.assertEqual(im.ndim, ndim)

    def _test(self, path, info, n):
        """
        :param path: Path to CSV with image (data) tuples.
        :param info: Iterable with image info tuples in form of (dtype, ndim), only the first record is checked.
        """
        d = data.Data(path)
        # __len__
        self.assertTrue(len(d))
        # __getitem__
        images = d[0]
        self.assertEqual(len(images), len(info))
        for im, (dtype, ndim) in zip(images, info):
            self._check_type_and_size(im, dtype, ndim)
        # __iter__
        i = 0
        for images in d:
            if i == n:
                break
            i += 1
            for im, (dtype, ndim) in zip(images, info):
                self._check_type_and_size(im, dtype, ndim)

    def test_local(self):
        path = os.path.join(os.path.dirname(__file__), 'tuples.txt')
        self._test(path, ((np.uint8, 3), (np.uint16, 2), (np.uint8, 2)), n=None)

    def test_remote(self):
        path = 'http://ptak.felk.cvut.cz/tradr/data/human_seg/composed_trn/annot.txt'
        self._test(path, ((np.uint8, 3), (np.uint16, 2), (np.uint16, 2), (np.uint8, 2)), n=2)

    def test_invalid_local(self):
        with self.assertRaises(IOError):
            data.Data('non-existing.csv')

    def test_invalid_remote(self):
        with self.assertRaises(requests.ConnectionError):
            data.Data('http://non-existing-domain/non-existing.csv')
    
    def test_repeated(self):
        path = os.path.join(os.path.dirname(__file__), 'tuples.txt')
        d = data.Repeated(data.Data(path), n=3, shuffle=True)
        with self.assertRaises(AttributeError):
            d[0]
        with self.assertRaises(AttributeError):
            len(d)
        n = 0
        for _ in d:
            n += 1
        self.assertEqual(n, 3*len(d.data))


if __name__ == '__main__':
    unittest.main()
